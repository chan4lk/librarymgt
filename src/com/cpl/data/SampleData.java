package com.cpl.data;

import java.util.ArrayList;
import java.util.Date;

public class SampleData {
	
	/**
	 * Number of books.
	 */
	private static final int numofBooks = 80;
	
	/**
	 * Number of copies per title.
	 */
	private static final int numofCopies = 5;
	
	/**
	 * Number of readers.
	 */
	private static final int numofReaders = 20;
	
	/**
	 * Number of logins.
	 */
	private static final int numofLogins = 2;
	
	/**
	 * The default serial version.
	 */
	public static final int defaultVersion = 0;
	
	/**
	 * Default number of reservations.
	 */
	private static final int numofReservations = 2;
	
	/**
	 * The books list.
	 */
	private ArrayList<Book> books;
	
	/**
	 * The reader list.
	 */
	private ArrayList<Reader> readers;
	
	/**
	 * The login list.
	 */
	private ArrayList<Login> logins;
	
	/**
	 * This to maintain list of reservations.
	 */
	private ArrayList<Reservation> reservations;
	
	/**
	 * The loan records.
	 */
	private ArrayList<Loan> loans;
	
	/**
	 * The return records.
	 */
	private ArrayList<Return> returns;
	
	/**
	 * The payment records.
	 */
	private ArrayList<Payment> payments;
	
	/**
	 * The singleton instance.
	 */
	private static SampleData instance = new SampleData();;

	/**
	 * The constructor.
	 */
	private SampleData() {
		this.initBooks();
		this.initLogins();
		this.initReaders();
		this.initReservations();
		this.initLoans();
		this.initReturns();
		this.initPayments();
	}
	
	/**
	 * initialize the books.
	 */
	private void initBooks()
	{
		this.books = new ArrayList<Book>();
		
		for (int i = 0; i < SampleData.numofBooks / SampleData.numofCopies; i++) {
					
			for (int j = 1; j <= SampleData.numofCopies; j++) {
				
				int index = SampleData.numofCopies * i + j;
				
				Book book = new Book();
				book.setId(String.format("B%3d", index));
				book.setTitle(String.format("Title%2d", i+1));
				book.setAuthor(String.format("Author%2d", i+1));
				book.setPublisher(String.format("Publisher%2d", i+1));
				book.setBorrowed(false);
				book.setStatus(BookStatus.DEFAULT);
				book.setReserved(false);
				book.setDeleted(false);
				book.setVersion(SampleData.defaultVersion);
				this.books.add(book);
			}
		}
	}
	
	/**
	 * initialize the readers.
	 */
	private void initReaders()
	{
		this.readers = new ArrayList<Reader>();
		
		for (int i = 0; i < SampleData.numofReaders; i++) {
			
			Reader reader = new Reader();
			reader.setId(String.format("R%3d", i));
			reader.setFirstName(String.format("Reader %2d", i));
			reader.setLastName(String.format("Doe %2d", i));
			reader.setVersion(SampleData.defaultVersion);
			this.readers.add(reader);
			
		}
	}
	
	/**
	 * initialize the logins.
	 */
	private void initLogins()
	{
		this.logins = new ArrayList<Login>();
		
		for (int i = 0; i < SampleData.numofLogins; i++) {
			Login login = new Login();
			login.setId(String.format("John %2d", i));
			login.setFirstName(String.format("Log %2d", i));
			login.setLastName(String.format("Doe %2d", i));
			login.setUserName(String.format("User%d", i));
			login.setPassword("123321");
			login.setVersion(SampleData.defaultVersion);
			this.logins.add(login);
		}
	}
	
	/**
	 * Initialize reservations.
	 */
	private void initReservations()  {
		
		this.reservations = new ArrayList<Reservation>();
		
		for (int i = 0; i < SampleData.numofReservations; i++) {
			Reservation reserved = new Reservation();
			reserved.setId(String.format("REID %2d", i));
			reserved.setVersion(SampleData.defaultVersion);
			reserved.setMemberId(String.format("R%3d", i));
			reserved.setResDate(new Date());
			reserved.setBookTitle(String.format("Title %2d", i));
			reserved.setCopyId(String.format("B%3d", i));
			reserved.setReady(false);
			reserved.setClear(false);
			this.reservations.add(reserved);
					
						
		}
	}
	
	/**
	 * Initialize all the loans.
	 */
	private void initLoans()  {		
		this.loans = new ArrayList<Loan>();		
	}
	
	/**
	 * Initialize the return book records.
	 */
	private void initReturns()  {		
		this.returns = new ArrayList<Return>();		
	}
		
	/**
	 * Initialize the payment records.
	 */
	private void initPayments(){
		this.payments = new ArrayList<Payment>();
	}
	/**
	 * @return the books.
	 */
	public ArrayList<Book> getBooks() {
		return books;
	}

	/**
	 * @return the readers.
	 */
	public ArrayList<Reader> getReaders() {
		return readers;
	}

	/**
	 * @return the logins.
	 */
	public ArrayList<Login> getLogins() {
		return logins;
	}
	
	/**
	 * @return the reservations.
	 */
	public ArrayList<Reservation> getReservations()
	{
		return this.reservations;
	}
	
	/**
	 * @return the loan records.
	 */
	public ArrayList<Loan> getLoans()
	{
		return this.loans;
	}
	
	/**
	 * @return the return book records.
	 */
	public ArrayList<Return> getReturns()
	{
		return this.returns;
	}
	
	/**
	 * @return The payment records.
	 */
	public ArrayList<Payment> getPayments() {
		return payments;
	}

	/**
	 * @return the single instance of the data.
	 */
	public static SampleData getInstance()
	{
		if (SampleData.instance == null) {
			SampleData.instance = new SampleData();
		}
		return SampleData.instance;
	}
	
	public void listBooks() {
		for (Book b : books) {
		System.out.println(
				String.format("%s %s %s %s deleted: %s",
						b.getId(),
						b.getTitle(),
						b.getAuthor(), 
						b.getPublisher(),
						b.isDeleted()));
	}
	
}
}
