package com.cpl.data;

/**
 * @author Chandima.Ranaweera
 *
 */
public class Login extends Person implements IDataModel {
	
	/**
	 * the identifier.
	 */
	private String id;
	
	/**
	 * the version
	 */
	private int version;
	
	/**
	 * the user name of the login
	 */
	private String userName;
	
	/**
	 * the password for the login
	 */
	private String password;
	
	
	
	/**
	 * @return the username
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName set the user name of the login.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password of the login
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password set the password of the login.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param id set the identifier of the login.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param version set the version of the login.
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/* (non-Javadoc)
	 * @see com.cpl.data.IDataModel#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see com.cpl.data.IDataModel#getVersion()
	 */
	@Override
	public int getVersion() {
		
		return this.version;
	}

}
