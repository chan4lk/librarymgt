package com.cpl.data;

public interface IDataModel {
	
	/**
	 * @return the id of the model
	 */
	String getId();
	
	/**
	 * @return the version of the model.
	 */
	int getVersion();
}
