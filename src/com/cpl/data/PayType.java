package com.cpl.data;

public enum PayType {

	Fine,
	
	/**
	 * member charged for exceeding book return 
	 * or damaged caused to books
	 */	
	MemberFee
}
