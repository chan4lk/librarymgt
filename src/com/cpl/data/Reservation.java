package com.cpl.data;

import java.util.Date;

/**
 * @author Administrator
 *
 */
public class Reservation implements IDataModel {

	/**
	 * This is to define reservation id.
	 */
	private String id;

	/**
	 * This to keep a track of version modification.
	 */
	private int version;

	/**
	 * This to verify copy of the book availability for the reservation request.
	 */
	private boolean isReady;

	/**
	 * This to track the reservation request.
	 */
	private Date resDate;

	/**
	 * Track the member who reserved.
	 */
	private String memberId;

	/**
	 * This is defining book title.
	 */
	private String bookTitle;

	/**
	 * Track the available copy id for the request.
	 */
	private String copyId;

	/**
	 * Reservation entry status.
	 */
	private boolean isClear;

	/* (non-Javadoc)
	 * @see com.cpl.data.IDataModel#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see com.cpl.data.IDataModel#getVersion()
	 */
	@Override
	public int getVersion() {
		return this.version;
	}

	/**
	 * @return Return the status of the reservation.
	 */
	public boolean isReady() {
		return isReady;
	}

	/**
	 * @param isReady Set the status of the reservation.
	 */
	public void setReady(boolean isReady) {
		this.isReady = isReady;
	}

	public Date getResDate() {
		return resDate;
	}

	public void setResDate(Date resDate) {
		this.resDate = resDate;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getCopyId() {
		return copyId;
	}

	/**
	 * @param copyId 
	 */
	public void setCopyId(String copyId) {
		this.copyId = copyId;
	}

	/**
	 * @return Get the clear flag.
	 */ 
	public boolean isClear() {
		return isClear;
	}

	/**
	 * @param isClear Set the clear flag.
	 */
	public void setClear(boolean isClear) {
		this.isClear = isClear;
	}

	/**
	 * @param id The record Identifier.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param version The record version.
	 */
	public void setVersion(int version) {
		this.version = version;
	}

}
