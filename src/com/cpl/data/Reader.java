/**
 * 
 */
package com.cpl.data;

/**
 * @author Chandima
 *
 */
public class Reader extends Person implements IDataModel {
	
	/**
	 * The identifier of the reader.
	 */
	private String id;
	
	/**
	 * The version of the reader.
	 */
	private int version;
	
	/* (non-Javadoc)
	 * @see com.cpl.data.IDataModel#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see com.cpl.data.IDataModel#getVersion()
	 */
	@Override
	public int getVersion() {
		// TODO Auto-generated method stub
		return this.version;
	}

	/**
	 * @param id set the identifier of the reader.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param version set the version of the reader.
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	
}
