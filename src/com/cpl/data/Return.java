package com.cpl.data;

import java.util.Date;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 */
public class Return implements IDataModel {
	
	private String readerId;
	
	private String bookId;
	
	private Date returnDate;
	
	private BookStatus bookStatus;
	
	private double fineAmount;
	
	private String returnId;
	
	private int version;
		
	@Override
	public String getId() {
		return returnId;
	}

	public void setId(String id) {
		this.returnId = id;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public BookStatus getBookStatus() {
		return bookStatus;
	}

	public void setBookStatus(BookStatus bookStatus) {
		this.bookStatus = bookStatus;
	}

	public double getFineAmount() {
		return fineAmount;
	}

	public void setFineAmount(double fineAmount) {
		this.fineAmount = fineAmount;
	}

	public String getReaderId() {
		return readerId;
	}

	public void setReaderId(String readerId) {
		this.readerId = readerId;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public int getVersion() {
		return this.version;
	}

}
