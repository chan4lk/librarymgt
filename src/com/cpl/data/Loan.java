package com.cpl.data;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This class is for modeling book loan.
 */
/**
 *
 */
public class Loan implements IDataModel {

	/**
	 * identifier of the loan.
	 */
	private String id;
	
	/**
	 * version of the record.
	 */
	private int version;
	
	/**
	 * loaning date.
	 */
	private Date loanDate;
	
	/**
	 * return date.
	 */
	private Date returnDate;
	
	/**
	 * the book identifiers.
	 */
	private String book;
	
		
	/**
	 * borrower identifier.
	 */
	private String readerId;
	
	/**
	 * All the loaned books are returned.
	 */
	private boolean isClear;
	
	/* (non-Javadoc)
	 * @see com.cpl.data.IDataModel#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see com.cpl.data.IDataModel#getVersion()
	 */
	@Override
	public int getVersion() {
		return this.version;
	}

	/**
	 * @return the loan date.
	 */
	public Date getLoanDate() {
		return loanDate;
	}

	/**
	 * @param loanDate set the loan date.
	 */
	public void setLoanDate(Date loanDate) {
		this.loanDate = loanDate;
	}

	/**
	 * @return the books list.
	 */
	public String getBook() {
		return book;
	}

	/**
	 * @param books set books.
	 */
	public void setBook(String book) {
		this.book = book;
	}
	
	

	/**
	 * @return The reader identifier.
	 */
	public String getReaderId() {
		return readerId;
	}

	/**
	 * @param readerId set the reader.
	 */
	public void setReaderId(String readerId) {
		this.readerId = readerId;
	}

	/**
	 * @param id The record identifier.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param version the record version.
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/**
	 * @return Is all the books are returned.
	 */
	public boolean isClear() {
		return isClear;
	}

	/**
	 * @param isClear Set if all book are returned.
	 */
	public void setClear(boolean isClear) {
		this.isClear = isClear;
	}
	
	

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	
	
}
