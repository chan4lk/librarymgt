package com.cpl.data;

import java.util.Date;

public class Payment implements IDataModel{

	private String paymentId;
	
	private String readerId;
	
	private Date paymentDate;
	
	private double Amount;
	
	private PayType payType;
	
	private int version;
	
	private String bookId;
	
	private boolean paymentMade;
	
	
	public void setId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getReaderId() {
		return readerId;
	}

	public void setReaderId(String readerId) {
		this.readerId = readerId;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

	public PayType getPayType() {
		return payType;
	}

	public void setPayType(PayType payType) {
		this.payType = payType;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	
	public boolean isPaymentMade() {
		return paymentMade;
	}

		@Override
	public String getId() {
		return this.paymentId;
	}

	@Override
	public int getVersion() {
		return this.version;
	}

	
}
