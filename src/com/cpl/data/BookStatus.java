package com.cpl.data;

/**
 * @author Chandima.Ranaweera.
 * The status of the book.
 */
public enum BookStatus {
	/**
	 * The default status.
	 */
	DEFAULT,
	
	/**
	 * The book is damaged.
	 */
	DAMAGED,
	
	/**
	 * The book has changed.
	 */
	CHANGED
}
