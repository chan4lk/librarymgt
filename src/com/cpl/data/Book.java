package com.cpl.data;

/**
 * @author Chandima.Ranaweera
 *
 */
public class Book implements IDataModel {

	/**
	 * identifier of the book
	 */
	private String id;
	
	/**
	 * version of the book.
	 */
	private int version;
	
	/**
	 * title of the book
	 */
	private String title;
	
	/**
	 * author of the book
	 */
	private String author;
	
	/**
	 * publisher of the book.
	 */
	private String publisher;
	
	/**
	 * book is borrowed by someone.
	 */
	private boolean isBorrowed;
	
	/**
	 * book is reserved by someone.
	 */
	private boolean isReserved;
	
	/**
	 * set to true id book is deleted.
	 */
	private boolean isDeleted;
	
	/**
	 * The book status.
	 */
	private BookStatus status;
	
	/* (non-Javadoc)
	 * @see com.cpl.data.IDataModel#getId()
	 */
	@Override
	public String getId() {
		
		return this.id;
	}
	
	/**
	 * @param id set the identifier.
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the title of the book.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title set the title of the book.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	/**
	 * @param author set the author of the book.
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return true if book is reserved.
	 */
	public boolean isReserved() {
		return isReserved;
	}

	/**
	 * @param isReserved reserve the book.
	 */
	public void setReserved(boolean isReserved) {
		this.isReserved = isReserved;
	}

	
	/**
	 * @param version modified times.
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/* (non-Javadoc)
	 * @see com.cpl.data.IDataModel#getVersion()
	 */
	@Override
	public int getVersion() {
		
		return this.version;
	}

	/**
	 * @return the publisher of the book.
	 */
	public String getPublisher() {
		return publisher;
	}

	/**
	 * @param publisher modify the publisher of book.
	 */
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	/**
	 * @return true of book is borrowed otherwise false.
	 */
	public boolean isBorrowed() {
		return isBorrowed;
	}

	/**
	 * @param isBorrowed borrow the book.
	 */
	public void setBorrowed(boolean isBorrowed) {
		this.isBorrowed = isBorrowed;
	}

	/**
	 * @return true if book is deleted.
	 */
	public boolean isDeleted() {
		return isDeleted;
	}

	/**
	 * @param isDeleted set to true when deleting the book.
	 */
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * @return the status of the book.
	 */
	public BookStatus getStatus() {
		return status;
	}

	/**
	 * @param status set the status of the book.
	 */
	public void setStatus(BookStatus status) {
		this.status = status;
	}

}
