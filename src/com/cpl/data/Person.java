/**
 * 
 */
package com.cpl.data;

/**
 * @author Chandima
 *
 */
public abstract class Person implements IDataModel {
	
	/**
	 * The first name of the person
	 */
	protected String firstName;
	
	/**
	 * the last name of the person
	 */
	protected String lastName;
	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	
}
