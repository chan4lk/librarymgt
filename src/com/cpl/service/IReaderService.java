package com.cpl.service;

import java.util.ArrayList;

import com.cpl.data.Reader;

public interface IReaderService {
	
	/**
	 * @param id Identifier of the reader.
	 * @return Reader of the identifier.
	 */
	Reader getById(String id);
	
	/**
	 * @param firstName First Name of the reader
	 * @param lastName Last Name of the reader.
	 * @return
	 */
	String addReader(String firstName, String lastName);

	/**
	 * @param reader The reader.
	 * @return True if success.
	 */
	boolean updateReader(Reader reader);
	
	/**
	 * @param id Identifier of the reader.
	 * @return True if success.
	 */
	boolean deleteReader(String id);
	
	/**
	 * @param id Identifier of the reader.
	 * @return True if success.
	 */
	boolean deleteReader(int id);

	/**
	 * @param rowId Row Id of the reader.
	 * @return the Reader
	 */
	Reader getById(int rowId);

	/**
	 * @return The active reader List.
	 */
	ArrayList<Reader> getActiveReaders();

	/**
	 * @return The header column names
	 */
	String[] getHeaderColumns();

	/**
	 * Print list of readers.
	 */
	void listReaders();

}
