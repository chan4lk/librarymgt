package com.cpl.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import com.cpl.data.Book;
import com.cpl.data.BookStatus;
import com.cpl.data.SampleData;

@SuppressWarnings("unused")
public class BookMgtService implements IBookMgtService {

	private static final int defaultVersion = 0;
	
	@Override
	public String addbook(String title, String author, String publisher) {
		int count = SampleData.getInstance().getBooks().size();
		Book book = new Book();
		book.setTitle(title);
		book.setAuthor(author);
		book.setPublisher(publisher);
		book.setBorrowed(false);
		book.setDeleted(false);
		book.setReserved(false);
		book.setStatus(BookStatus.DEFAULT);
		book.setId(String.format("B%3d", count+1));
		book.setVersion(BookMgtService.defaultVersion);
		SampleData.getInstance().getBooks().add(book);
		SampleData.getInstance().listBooks();
		return book.getId();
	}

	@Override
	public boolean updateBook(Book book) {
		Book originalBook;
		try {
			ArrayList<Book> results = new ArrayList<Book>();
		for (Book b: SampleData.getInstance().getBooks()){
			
			if(b.getId().equals(book.getId())){
				results.add(b);
				
			}
		}
		if (results.size()>0){
			originalBook = results.get(0);
			originalBook.setAuthor(book.getAuthor());
			originalBook.setTitle(book.getTitle());
			originalBook.setPublisher(book.getPublisher());
			originalBook.setBorrowed(book.isBorrowed());
			originalBook.setDeleted(book.isDeleted());
			originalBook.setReserved(book.isReserved());
			originalBook.setVersion(originalBook.getVersion()+1);
		}else{
			 return false;
		 }
				
			
		}catch(NoSuchElementException ex){
			System.out.println("No such book found for id: " + book.getId());
			return false;
		}
		return true;
	}

	@Override
	public boolean deleteBook(String id) {
		Book originalBook;
		ArrayList<Book> deleteBooks = new ArrayList<Book> ();
		for (Book b : SampleData.getInstance().getBooks()){
          if(b.getId().equals(id)){
            deleteBooks.remove(b);
          }
		}
          
          if (deleteBooks.size()>0){
        	  originalBook = deleteBooks.get(0);
        	  originalBook.setDeleted(true);
          }else{
        	  return false;
          }
			
				
		return true;
	}

	@Override
	public String borrowBook(String bookId, String readerId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String reserveBook(String bookId, String readerId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.cpl.service.IBookMgtService#getBookById(java.lang.String)
	 */
	@Override
	public Book getById(String id) {
		Book originalBook = null;
		ArrayList<Book> getById = new ArrayList<Book>();
		for (Book b : SampleData.getInstance().getBooks()){
        	  if(b.getId().equals(id)){
        		  getById.add(b);
        	  }
          }
        	  
        	  if (getById.size()>0){
        		  originalBook = getById.get(0);
        		  return originalBook;
        	  }
        	  System.out.println("No Book Found with id:" + id);
        	  return originalBook;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IBookMgtService#DeleteBook(int)
	 */
	@Override
	public boolean deleteBook(int id) {
		Book originalBook = SampleData.getInstance().getBooks().get(id);
		
		if (originalBook == null || originalBook.isDeleted()) {
			return false;
		}
		
		originalBook.setDeleted(true);		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IBookMgtService#listBooks()
	 */
	@Override
	public void listBooks() {
			for (Book b : SampleData.getInstance().getBooks()) {
			System.out.println(
					String.format("%s %s %s %s deleted: %s",
							b.getId(),
							b.getTitle(),
							b.getAuthor(), 
							b.getPublisher(),
							b.isDeleted()));
		}
		
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IBookMgtService#clearReserve(java.lang.String)
	 */
	@Override
	public void clearReserve(String bookId) {
		Book book = this.getById(bookId);
		if (book != null) {
			book.setReserved(false);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IBookMgtService#getActiveBooks()
	 */
	@Override
	public ArrayList<Book> getActiveBooks() {
		ArrayList<Book> books = new ArrayList<Book>();
				
		return SampleData.getInstance().getBooks();
	}
	
	/* (non-Javadoc)
	 * @see com.cpl.service.IBookMgtService#getActiveBooks()
	 */
	@Override
	public ArrayList<Book> getAvailableBooks() {
		ArrayList<Book> books = new ArrayList<Book>();
		
		for (Book book : SampleData.getInstance().getBooks()) {
			if (book.isDeleted()== false && book.isBorrowed() == false) {
				books.add(book);
			}
		}
		
		return books;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IBookMgtService#getBookColums()
	 */
	@Override
	public String[] getBookColums() {
		String[] columnNames = {
				"ID",
				"Title",
                "Author",
                "Publisher",
                "Reserved",
                "Borrowed",
                "Deleted"};
		
		return columnNames;
	}

	@Override
	public ArrayList<Book> getByTitlte() {
		List<Book> originalBooks = null;
		
		try{
			originalBooks =	this.getAvailableBooks();
		}catch(NoSuchElementException ex){
			System.out.println("No such book found");
			
			return null;
		}
		
		return (ArrayList<Book>) originalBooks;
	}

	@Override
	public ArrayList<Book> getBooksOnLoan() {
		ArrayList<Book> books = new ArrayList<Book>();
		
		for (Book book : SampleData.getInstance().getBooks()) {
			if (!book.isDeleted() && book.isBorrowed()) {
				books.add(book);
			}
		}
		
		return books;
	}

}
