package com.cpl.service;

import java.util.ArrayList;

import com.cpl.data.Book;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 */
public interface IBookMgtService {
	
	/**
	 * @param book new book to add.
	 * @return id of the book.
	 */
	String addbook(String title, String author, String publisher);
	
	/**
	 * @param book details will be updated as given.
	 * @return true if book is updated otherwise false.
	 */
	boolean updateBook(Book book);
	
	/**
	 * @param id of the book to be deleted.
	 * @return true if book is deleted.
	 */
	boolean deleteBook(String id);
	
	/**
	 * @param id row of the book list.
	 * @return true if book is deleted.
	 */
	boolean deleteBook(int id);
	
	/**
	 * @param bookId id of the book to be borrowed.
	 * @param readerId id of the reader who is borrowing.
	 * @return id of the borrow entry.
	 */
	String borrowBook(String bookId, String readerId);
	
	/**
	 * @param bookId id of the book.
	 * @param readerId id of the reader.
	 * @return id of the reserve entry.
	 */
	String reserveBook(String bookId, String readerId);

	/**
	 * @param id of the book.
	 * @return book of given id.
	 */
	Book getById(String id);
	
	/**
	 * Print all books list.
	 */
	void listBooks();
	
	/**
	 * @return The non deleted books;
	 */
	ArrayList<Book> getActiveBooks();

	/**
	 * @param bookId Identifier of the book;
	 */
	void clearReserve(String bookId);

	/**
	 * @return The book column names for book frame table.
	 */
	String[] getBookColums();

	/**
	 * @return Get books list by distinct titles.
	 */
	ArrayList<Book> getByTitlte();

	/**
	 * @return Books which are already in the library.
	 */
	ArrayList<Book> getAvailableBooks();

	/**
	 * @return Return books on loan.
	 */
	ArrayList<Book> getBooksOnLoan();
	
}
