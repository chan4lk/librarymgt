package com.cpl.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.NoSuchElementException;

import com.cpl.data.Book;
import com.cpl.data.Reservation;
import com.cpl.data.SampleData;

public class ReservationService implements IReservationService {
	
	/* (non-Javadoc)
	 * @see com.cpl.service.IReservationService#GetById(java.lang.String)
	 */
	public Reservation GetById(String id) {
		Reservation reservation = null;
		ArrayList<Reservation> results = new ArrayList<Reservation>();
		for (Reservation r : SampleData.getInstance().getReservations()){
        	  if(r.getId().equals(id)){
        		  results.add(r);
        	  }
          }
        	  
        	  if (results.size()>0){
        		  reservation = results.get(0);
        	  }else{
        		  System.out.println("No Reservation Found with id:" + id);
        	  }
        	  
		return reservation;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IReservationService#reserveBook(java.lang.String)
	 */
	@Override
	public String reserveBook(String title, String reader) {	
		
		Reservation reservation = null;
		Book book = null;
		
		try{
			
			ArrayList<Book> results = new ArrayList<Book>();
			   
		     for(Book b : SampleData.getInstance().getBooks()){
		          if(b.getTitle().equals(title) && 
		        		  b.isReserved() == false &&
		        		  b.isBorrowed() == true &&
		        		  b.isDeleted() == false ){
		            results.add(b);
		          }
		     }
		      if(results.size()>0){
		    	  book = results.get(0);
					}
			if (book != null) {
				
				book.setReserved(true);
				reservation = new Reservation();
				reservation.setId(String.format("REID %2d", SampleData.getInstance().getReservations().size()));
				reservation.setBookTitle(book.getTitle());
				reservation.setCopyId(book.getId());
				reservation.setMemberId(reader);
				reservation.setReady(false);
				reservation.setResDate(new Date());
				reservation.setVersion(SampleData.defaultVersion);
				SampleData.getInstance().getReservations().add(reservation);
			}
			
			
		}catch(NoSuchElementException ex){
			System.out.println("No such book found for title: " + title);
			
			return null;
		}
		
		return reservation.getId();
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IReservationService#clearReserve(java.lang.String)
	 */
	@Override
	public boolean clearReserve(String id) {
		Reservation res = this.GetById(id);
		if (res != null) {
			String bookId = res.getCopyId();
			res.setClear(true);
			
			IBookMgtService bookService = new BookMgtService();
			bookService.clearReserve(bookId);
			
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IReservationService#listReservations()
	 */
	@Override
	public void listReservations() {
		for (Reservation r : SampleData.getInstance().getReservations()) {
			String.format("%s %s %s %s",
					r.getId(),
					r.getBookTitle(),
					r.getResDate(), 
					r.isClear());
		}
	}

}
