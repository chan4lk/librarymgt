package com.cpl.service;

import java.util.ArrayList;
import java.util.Date;

import com.cpl.data.Book;
import com.cpl.data.Loan;
import com.cpl.data.Reader;
import com.cpl.data.SampleData;

/**
 * @author Mohamed.Shamin
 * @version 1.0
 */
public class LoanService implements ILoanService {
	
	/**
	 * Default loan period;
	 */
	public static final int DefaultLoanPeriod = 5;

	/* (non-Javadoc)
	 * @see com.cpl.service.ILoanService#addLoan(java.lang.String, java.lang.String)
	 */
	@Override
	public String addLoan(ArrayList<String> books, String readerId, ArrayList<Date> returnDates) {
		String loanId = "";
		
		IBookMgtService bookService = ServiceFactory.getBookMgtService();
		IReaderService readerService = ServiceFactory.getReaderService();
		
		Reader reader = readerService.getById(readerId);
		
		
		int index = 0;
		for (String bookId : books) {
			Book book = bookService.getById(bookId);
			if (book != null && book.isBorrowed() == false && book.isDeleted() == false){
				
				if (reader != null) {	
					Loan loan = new Loan();
					loan.setBook(bookId);
					loan.setId(String.format("L%3d", SampleData.getInstance().getLoans().size()));
					loan.setReaderId(readerId);
					loan.setLoanDate(new Date());
					loan.setReturnDate(returnDates.get(index));
					loan.setVersion(SampleData.defaultVersion);
					SampleData.getInstance().getLoans().add(loan);
					loanId = loan.getId();
				}
				book.setBorrowed(true);
			} 
			index++;
		}
		
		
		
		return loanId;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.ILoanService#clearLoan(java.lang.String)
	 */
	@Override
	public boolean clearLoan(String loanId) {
		
		IBookMgtService bookService = ServiceFactory.getBookMgtService();
		Loan loan = this.getById(loanId);
		
		boolean clear = true;
		
		if (loan != null) {
				
				Book book = bookService.getById(loan.getBook());
				
				if (book != null && book.isBorrowed() == true) {
					clear = false;
				}
				
		
			if (clear) {
				loan.setClear(true);
				return true;
			}
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.ILoanService#deleteLoan(java.lang.String)
	 */
	@Override
	public boolean deleteLoan(String loanId) {
		Loan loan = this.getById(loanId);
		if (loan != null) {
			SampleData.getInstance().getLoans().remove(loan);
			return true;
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.ILoanService#getById(java.lang.String)
	 */
	@Override
	public Loan getById(String id) {
		Loan loan = null;
		for (Loan l : SampleData.getInstance().getLoans()) {
				if (l.getId() == id) {
					loan = l;
				}
			}
		if (loan == null) {
			System.out.println("No Loan found for id " + id);
		}
		return loan;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.ILoanService#listLoans()
	 */
	@Override
	public void listLoans() {
		for (Loan l : SampleData.getInstance().getLoans()) {
			System.out.printf("%s %s %s %s",
		
				l.getId(),
				l.getLoanDate(),
				l.getReturnDate(),
				l.getReaderId());
		}
		
	}
	
	/**
	 * @return column names
	 */
	@Override
	public String[] getColumnNames() {
		String[] columns = {"ID", "Titles", "Loan Date", "Period", "Reader"};
		
		return columns;
	}

	/**
	 * @return The active loans
	 */
	@Override
	public ArrayList<Loan> getActiveLoans() {
		
		return SampleData.getInstance().getLoans();
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.ILoanService#deleteLoan(int)
	 */
	@Override
	public boolean deleteLoan(int selected) {
		boolean success = false;
		try {
			Loan loan = SampleData.getInstance().getLoans().get(selected);
			 success = this.deleteLoan(loan.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return success;
	}

}
