package com.cpl.service;

import java.util.ArrayList;
import java.util.Date;

import com.cpl.data.Loan;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 */
public interface ILoanService {
	
	/**
	 * @param books Identifiers of the Book.
	 * @param returnDate Identifiers return date of the Book
	 * @param readerId Identifier of the Reader.
	 * @return Identifier of the new record.
	 */
	String addLoan(ArrayList<String> books, String readerId, ArrayList<Date> returnDate);
	
	/**
	 * @param id Identifier of the loan.
	 * @return True if clear is success.
	 */
	boolean clearLoan(String loanId);
	
	/**
	 * @param loanId Identifier of the loan.
	 * @return True if delete success.
	 */
	boolean deleteLoan(String loanId);
	
	/**
	 * @param id Identifier of the loan
	 * @return The loan.
	 */
	Loan getById(String id);
	
	/**
	 * Print all loans.
	 */
	void listLoans();

	/**
	 * @return The column names list.
	 */
	String[] getColumnNames();

	/**
	 * @return Return the active loans list.
	 */
	ArrayList<Loan> getActiveLoans();

	/**
	 * @param selected Selected index.
	 * @return True if deletion success.
	 */
	boolean deleteLoan(int selected);
	
}
