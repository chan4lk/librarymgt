package com.cpl.service;

import java.util.ArrayList;
import java.util.Date;

import com.cpl.data.Book;
import com.cpl.data.BookStatus;
import com.cpl.data.Loan;
import com.cpl.data.Return;
import com.cpl.data.SampleData;

public class ReturnService implements IReturnService {
	
	@Override
	public String addReturn(String readerId, String bookId,
			BookStatus bookStatus, double fineAmount) {
		
		String returnId = "";
		
		IBookMgtService bookService = ServiceFactory.getBookMgtService();
		Book rb = bookService.getById(bookId);
		rb.setBorrowed(false);
		
		ILoanService loanService = ServiceFactory.getLoanService();
		ArrayList<Loan> loans = loanService.getActiveLoans();
		for (Loan ln : loans) {
			if (ln.getReaderId().equals(readerId)&& ln.getBook().equals(bookId) && ln.isClear()== false){
				Return record = new Return();
				record.setBookId(bookId);
				record.setReaderId(readerId);
				record.setBookStatus(bookStatus);
				record.setFineAmount(fineAmount);
				record.setId(String.format("RE%3d",SampleData.getInstance().getReturns().size()));
				record.setReturnDate(new Date());
				record.setVersion(SampleData.defaultVersion);
				ln.setClear(true);
				SampleData.getInstance().getReturns().add(record);
				returnId = record.getId();
			}
			
		}
		
			
		return returnId; 
	}

	@Override
	public void listReturns() {
		for (Return r : SampleData.getInstance().getReturns()) {
			System.out.printf("%s %s %s",
					r.getId(),
					r.getReturnDate(),
					r.getReaderId());
		}
	}

	@Override
	public Return getById(String id) {
		Return record = null;
		
		ArrayList<Return> results = new ArrayList<Return>();
		for (Return r : SampleData.getInstance().getReturns()){
        	  if(r.getId().equals(id)){
        		  results.add(r);
        	  }
          }
        	  
        	  if (results.size()>0){
        		  record = results.get(0);
        	  }else{
        		  System.out.println("No Return Found with id:" + id);
        	  }
        	  
		return record;
	}
	
	@Override
	public ArrayList<Return> getActiveReturns() {
		ArrayList<Return> records = SampleData.getInstance().getReturns();
		
		return records;
	}

	@Override
	public String[] getHeaderColumns() {
		String[] header = {
			"ID",
			"Book ID",
			"Reader ID",
			"Date",
			"Fine"
		};
		
		return header;
	}

	@Override
	public boolean deleteReturn(int selected) {
		boolean success = false;
		try{
			Return record = SampleData.getInstance().getReturns().get(selected);
			success = this.deleteReturn(record.getId());
		}catch(Exception exp){
			System.out.println("No record found for row " + selected);
			
		}
		
		return success;
	}

	@Override
	public boolean deleteReturn(String id) {
		boolean success = false;
		try{
			Return record = getById(id);
			success = SampleData.getInstance().getReturns().remove(record);
		}catch(Exception exp){
			System.out.println("No record found for id " + id);
			
		}
		
		return success;
	}

	
	
	
}
