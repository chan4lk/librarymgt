package com.cpl.service;

import com.cpl.data.PayType;

public interface IPaymentService {

	/**
	 * @param bookId Id of the book if fine is paid.
	 * @param readerId Id of the reader who pays.
	 * @param amount Amount of payment.
	 * @param type Fee or Member charges
	 * @return Id of the new record.
	 */
	String addPayment(String bookId, String readerId, double amount, PayType type, boolean paymentMade);
	
	/**
	 * Print all payments
	 */
	void listPayments();
	
}
