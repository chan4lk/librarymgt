package com.cpl.service;

public class ServiceFactory {
	
	/**
	 * @return Book management service.
	 */
	public static IBookMgtService getBookMgtService() {
		return new BookMgtService();
	}
	
	/**
	 * @return Loan management service.
	 */
	public static ILoanService getLoanService() {
		return new LoanService();
	}
	
	/**
	 * @return Security service.
	 */
	public static ISecurityService getSecurityService(){
		return new SecurityService();
	}
	
	/**
	 * @return Reservation Service.
	 */
	public static IReservationService getIReservationService(){
		return new ReservationService();
	}
	
	/**
	 * @return The return service. This is used to return a borrowed book.
	 */
	public static IReturnService getReturnService() {
		return new ReturnService();
	}
	
	/**
	 * @return The IReaderService instance.
	 */
	public static IReaderService getReaderService() {
		return new ReaderService();
	}

	/**
	 * @return The IPaymentService instance.
	 */
	public static IPaymentService getPaymentService() {
		return new PaymentService();
	}
}
