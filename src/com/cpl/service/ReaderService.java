package com.cpl.service;

import java.util.ArrayList;

import com.cpl.data.Reader;
import com.cpl.data.SampleData;

public class ReaderService implements IReaderService {

	/* (non-Javadoc)
	 * @see com.cpl.service.IReaderService#getById(java.lang.String)
	 */
	@Override
	public Reader getById(String id) {
		Reader reader = null;
		
		for (Reader r : SampleData.getInstance().getReaders()) {
			if (r.getId().equals(id)) {
				reader = r;
			}
		}
		if (reader == null) {
			System.out.printf("No reader found for id %s", id);		
		}
		
		return reader;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IReaderService#addReader(java.lang.String, java.lang.String)
	 */
	@Override
	public String addReader(String firstName, String lastName) {
		String id = "";
		
		Reader reader = new Reader();
		reader.setId(String.format("R%03d",SampleData.getInstance().getReaders().size()));
		reader.setFirstName(firstName);
		reader.setLastName(lastName);
		reader.setVersion(SampleData.defaultVersion);
		
		SampleData.getInstance().getReaders().add(reader);
		
		id = reader.getId();
		
		return id;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IReaderService#updateReader(com.cpl.data.Reader)
	 */
	@Override
	public boolean updateReader(Reader reader) {
		Reader originalReader = this.getById(reader.getId());
		
		if (originalReader != null) {
			originalReader.setFirstName(reader.getFirstName());
			originalReader.setLastName(reader.getLastName());
			originalReader.setVersion(originalReader.getVersion() + 1);
			return true;
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IReaderService#deleteReader(java.lang.String)
	 */
	@Override
	public boolean deleteReader(String id) {
		Reader reader = this.getById(id);
		if (reader != null) {
			SampleData.getInstance().getReaders().remove(reader);
			return true;
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IReaderService#deleteReader(int)
	 */
	@Override
	public boolean deleteReader(int id) {
		boolean success = false;
		try {
			Reader reader =  SampleData.getInstance().getReaders().get(id);
			String readerId = reader.getId();
			this.deleteReader(readerId);
		} catch (Exception e) {
			System.out.println("No reader found for selected row" + id);
		}
		
		return success;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IReaderService#getById(int)
	 */
	@Override
	public Reader getById(int rowId) {
		Reader reader;
		try {
			reader =  SampleData.getInstance().getReaders().get(rowId);
			
		} catch (Exception e) {
			
			return null;
		}
		
		return reader;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IReaderService#getActiveReaders()
	 */
	@Override
	public ArrayList<Reader> getActiveReaders() {		
		return SampleData.getInstance().getReaders();
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IReaderService#getHeaderColums()
	 */
	@Override
	public String[] getHeaderColumns() {
		String[] columnNames = {"ID", "First Name", "Last Name"};		
		return columnNames;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IReaderService#listReaders()
	 */
	@Override
	public void listReaders() {
		for (Reader r : SampleData.getInstance().getReaders()) {
			System.out.printf(
					"%s %s %s",
					r.getId(),
					r.getFirstName(),
					r.getLastName());
		}
		
	}

}
