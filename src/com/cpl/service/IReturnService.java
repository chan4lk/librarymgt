/**
 * 
 */
package com.cpl.service;

import java.util.ArrayList;

import com.cpl.data.BookStatus;
import com.cpl.data.Return;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 */
public interface IReturnService {
	
	String addReturn(String memberId, String bookId, BookStatus bookStatus, double fineAmount);
	
	Return getById(String id);
	
	void listReturns ();

	/**
	 * @return Get the Active returns
	 */
	ArrayList<Return> getActiveReturns();
	
	/**
	 * @return Get Table headers.
	 */
	String[] getHeaderColumns();

	/**
	 * @param selected Selected row number.
	 * @return True if deleted.
	 */
	boolean deleteReturn(int selected);
	
	/**
	 * @param id Identifier of the return
	 * @return true if success.
	 */
	boolean deleteReturn(String id);
	
}
