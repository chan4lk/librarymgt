package com.cpl.service;

import java.util.Date;

import com.cpl.data.PayType;
import com.cpl.data.Payment;
import com.cpl.data.SampleData;

public class PaymentService implements IPaymentService {

	/* (non-Javadoc)
	 * @see com.cpl.service.IPaymentService#addPayment(java.lang.String, java.lang.String, double, com.cpl.data.PayType)
	 */
	@Override
	public String addPayment(String bookId, String readerId, double amount, PayType type, boolean paymentMade) {
		String payId = "";
		
		Payment payment = new Payment();
		payment.setId(String.format("PY%03d",SampleData.getInstance().getPayments().size()));
		payment.setAmount(amount);
		payment.setBookId(bookId);
		payment.setPayType(type);
		payment.setPaymentDate(new Date());
		payment.setReaderId(readerId);
		payment.isPaymentMade();
		payment.setVersion(SampleData.defaultVersion);
		SampleData.getInstance().getPayments().add(payment);
		
		payId = payment.getId();
		
		return payId;
	}

	/* (non-Javadoc)
	 * @see com.cpl.service.IPaymentService#listPayments()
	 */
	@Override
	public void listPayments() {
		for (Payment p : SampleData.getInstance().getPayments()){
			System.out.printf(
				"%s %s %s %s",
				p.getId(),
				p.getPaymentDate(),
				p.getAmount(),
				p.getReaderId());
		}
	}
}
