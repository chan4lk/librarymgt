package com.cpl.service;

import com.cpl.data.Login;

/**
 * @author Chandima.Ranaweera
 *
 */
public interface ISecurityService {
	
	/**
	 * @param userName user name of the login
	 * @param password the password of the login
	 * @return true if login is success otherwise false.
	 */
	boolean SignIn(String userName, String password);
	
	/**
	 * @param login the login user.
	 * @return true if adding the login is success.
	 */
	boolean AddUser(Login login);
}
