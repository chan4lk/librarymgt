package com.cpl.service;

import com.cpl.data.Reservation;

public interface IReservationService {
	
	/**
	 * @param id Identifier of the reservation.
	 * @return Reservation of given id;
	 */
	Reservation GetById(String id);
	
	/**
	 * @param title The title of the book.
	 * @param reader The reserver of the book.
	 * @return id of the book.
	 */
	String reserveBook(String title, String reader);
	
	/**
	 * @param id of the book.
	 * @return True if clearing is success.
	 */
	boolean clearReserve(String id);
	
	/**
	 * Print all reservations.
	 */
	void listReservations();
}
