package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.cpl.service.BookMgtService;
import com.cpl.service.IBookMgtService;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This dialog will add a new book to the library.
 */
public class AddBookDialog extends JDialog {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 2309200214777748119L;
	
	/**
	 * The empty string.
	 */
	private static final String emptyString = "";
	
	
	/**
	 * The content panel.
	 */
	private final JPanel contentPanel = new JPanel();
	
	/**
	 * The title text field;
	 */
	private JTextField titleTextField;
	
	/**
	 * The author text field.
	 */
	private JTextField authorTextField;
	
	
	/**
	 * The publisher text field.
	 */
	private JTextField publisherTextField;
	
	/**
	 * Event to fire when book is added.
	 */
	private BookUpdateActionListener addEvent;

	/**
	 * Create the dialog.
	 * @param event This event fires when book is added.
	 */
	public AddBookDialog(BookUpdateActionListener event) {
		this.addEvent = event;
		setTitle("Add new book");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblTitle = new JLabel("Title");
			lblTitle.setBounds(76, 17, 86, 21);
			contentPanel.add(lblTitle);
		}
		{
			titleTextField = new JTextField();
			titleTextField.setBounds(187, 9, 144, 36);
			contentPanel.add(titleTextField);
			titleTextField.setColumns(10);
		}
		{
			JLabel lblAuthor = new JLabel("Author");
			lblAuthor.setBounds(76, 68, 86, 21);
			contentPanel.add(lblAuthor);
		}
		
		authorTextField = new JTextField();
		authorTextField.setBounds(187, 60, 144, 36);
		contentPanel.add(authorTextField);
		authorTextField.setColumns(10);
		
		JLabel lblPublisher = new JLabel("Publisher");
		lblPublisher.setBounds(76, 121, 86, 21);
		contentPanel.add(lblPublisher);
		
		publisherTextField = new JTextField();
		publisherTextField.setBounds(187, 113, 144, 36);
		contentPanel.add(publisherTextField);
		publisherTextField.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent event) {
						String title = titleTextField.getText();
						String author = authorTextField.getText();
						String publisher = publisherTextField.getText();
						
						addNewBook(title, author, publisher);
						
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						UIRenderer.hideAddBookDialog();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	/**
	 * @param title of the book.
	 * @param author of the book.
	 * @param publisher of the book.
	 */
	protected void addNewBook(String title, String author, String publisher) {
			IBookMgtService service = new BookMgtService();
			
			
			String id = service.addbook(title, author, publisher);
			
			if (!id.equalsIgnoreCase("")) {
				UIRenderer.hideAddBookDialog();
				this.addEvent.onAdd();
			}
	}

	/**
	 * Clear all inputs.
	 */
	public void clearInputs() {
		this.titleTextField.setText(emptyString);
		this.authorTextField.setText(emptyString);
		this.publisherTextField.setText(emptyString);
		
	}
}
