package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

public class MessageDialog extends JDialog {

	/**
	 * The default serial version id.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The content panel.
	 */
	private final JPanel contentPanel = new JPanel();
	
	/**
	 * The OK text.
	 */
	private static final String okText = "OK";

	/**
	 * Create the dialog.
	 * @param message Message to display in dialog.
	 */
	public MessageDialog(String message) {
		setTitle("Infomation");
		setBounds(100, 100, 450, 176);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			//Set label to message.
			JLabel lblMessage = new JLabel(message);
			contentPanel.add(lblMessage);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton(okText);
				okButton.setActionCommand(okText);
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						MessageDialog.this.dispose();						
					}
				});
			}
		}
	}

}
