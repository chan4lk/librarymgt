package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This will list all the book to a data grid.
 */
public class BookSelectFrame extends JFrame {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 4780997252713603189L;
	
	/**
	 * The content pane.
	 */
	private JPanel contentPane;
	private JPanel panel;
	private JButton btnSelectBook;
	private JTable bookTable;
	private BookTableModel model;
	private BookSelectListener updateEvent;
	private List<String> selectedTitles;
	
	/**
	 * Create the frame.
	 */
	public BookSelectFrame(ViewType type) {
		setTitle("Select Book");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 1200, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		model = new BookTableModel(type);
		bookTable = new JTable(model);
		
		JScrollPane scrollPane = new JScrollPane(bookTable);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.EAST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		btnSelectBook = new JButton("Select");
		btnSelectBook.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectd = bookTable.getSelectedRows();
				for (int i : selectd) {
					String id = (String) BookSelectFrame.this.model.getValueAt(i, 0);
					String title = (String) BookSelectFrame.this.model.getValueAt(i, 1);
					if (!selectedTitles.contains(title)) {
						BookSelectFrame.this.updateEvent.onBookSelect(id);
						selectedTitles.add(title);
					}
					
				}
				
				model.fireTableDataChanged();
				UIRenderer.hideBookSelectFrame();
			}
		});
		btnSelectBook.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnSelectBook);
		
	}
	
	/**
	 * initialize the frame.
	 */
	public void initialize(BookSelectListener updateEvent){
		this.updateEvent = updateEvent;
		this.selectedTitles = new ArrayList<String>();
	}

	public void clearInputs() {
				
	}
	
}
