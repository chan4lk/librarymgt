package com.cpl.view;

/**
 * @author Chandima.Ranaweera
 *
 */
public interface ReturnActionListener {
	/**
	 * On Return return list is updated. 
	 */
	void onUpdate();
}
