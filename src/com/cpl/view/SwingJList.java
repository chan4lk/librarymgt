package com.cpl.view;

import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;

@SuppressWarnings("serial")
public class SwingJList<T> extends JList {
	public static final int NEW_ELEMENT_IDX = 0;

	public SwingJList(ArrayList<T> listData) {

		// Create a JList data model
		super(new DefaultListModel());

		for (T t : listData) {
			addElement(t);
		}

		// Set selection mode
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

	}

	public void addElement(T element) {
		((DefaultListModel) getModel()).add(NEW_ELEMENT_IDX,
				element);
	}

	public void removeElement(Object element) {
		((DefaultListModel) getModel()).removeElement(element);
	}

}