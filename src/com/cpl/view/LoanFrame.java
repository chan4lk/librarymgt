package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import com.cpl.service.ILoanService;
import com.cpl.service.ServiceFactory;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This will list all the book to a data grid.
 */
public class LoanFrame extends JFrame implements LoanUpdateActionListener {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 4780997252713603189L;
	
	/**
	 * The content pane.
	 */
	private JPanel contentPane;
	private JTable loanTable;
	private JPanel panel;
	private JButton btnAddBook;
	private JButton btnDelete;
	private LoanTableModel model;

	/**
	 * Create the frame.
	 */
	public LoanFrame() {
		setTitle("Manage Loans");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 1200, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		model = new LoanTableModel();
		loanTable = new JTable(model);
		
		JScrollPane scrollPane = new JScrollPane(loanTable);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.EAST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		btnAddBook = new JButton("Add Loan");
		btnAddBook.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				UIRenderer.showAddLoanDialog(LoanFrame.this);				
			}
		});
		btnAddBook.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnAddBook);
		
		btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int selected = loanTable.getSelectedRow();
				if (selected == -1) {
					UIRenderer.showMessageDialog("Please selecte a record");
				}else{
					ILoanService service = ServiceFactory.getLoanService();
					boolean success = service.deleteLoan(selected);
					if (success) {
						LoanFrame.this.onDelete();
						service.listLoans();
					}
				}
			}
		});
		btnDelete.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnDelete);
		
	}
	
	/**
	 * initialize the frame.
	 */
	public void initialize(){
		
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onUpdate()
	 */
	@Override
	public void onUpdate() {
		System.out.println("On update Loan Frame.");
		model.fireTableDataChanged();		
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onDelete()
	 */
	@Override
	public void onDelete() {
		model.fireTableDataChanged();		
	}

	@Override
	public void onAdd() {
		System.out.println("On Add Loan Frame.");
		model.fireTableDataChanged();	
		
	}

}
