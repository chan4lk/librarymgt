package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import com.cpl.service.ILoanService;
import com.cpl.service.ServiceFactory;

import javax.swing.JTable;

/**
 * @author Mohamed.Shamin
 * @version 1.0
 * This dialog will add a new book to the library.
 */
public class AddLoanDialog extends JFrame implements LoanUpdateActionListener, ReaderSelectListener, BookSelectListener {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 2309200214777748119L;
	
	/**
	 * The empty string.
	 */
	private static final String emptyString = "";
	
	
	/**
	 * The content panel.
	 */
	private final JPanel contentPanel = new JPanel();
	
	/**
	 * The title text field;
	 */
	private JTextField readerTxt;
	
	/**
	 * Books List.
	 */
	private ArrayList<String> books;
	private ArrayList<Date> returnDates;
	
	/**
	 * Event to fire when book is added.
	 */
	private LoanUpdateActionListener addEvent;

	/**
	 * The selected book.
	 */
	private String selectedBook;
	private JTable table;
	private DefaultTableModel model;
	private JTextField daysTxt;

	/**
	 * Create the dialog.
	 * @param event This event fires when book is added.
	 */
	@SuppressWarnings("serial")
	public AddLoanDialog(LoanUpdateActionListener event) {
		this.initialize(event);
		
		setTitle("Add new Loan");
		setBounds(100, 100, 622, 386);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblReader = new JLabel("Reader ID");
			lblReader.setBounds(76, 17, 86, 21);
			contentPanel.add(lblReader);
		}
		{
			readerTxt = new JTextField();
			readerTxt.setBounds(141, 17, 157, 21);
			contentPanel.add(readerTxt);
			readerTxt.setColumns(10);
		}
		{
			JLabel lblBooks = new JLabel("Books");
			lblBooks.setBounds(76, 68, 86, 21);
			contentPanel.add(lblBooks);
		}
		
		JButton btnAddBook = new JButton("Add Book");
		
		btnAddBook.setBounds(458, 68, 109, 23);
		contentPanel.add(btnAddBook);
		{
			{
				JButton btnSelectReader = new JButton("Select Reader");
				btnSelectReader.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						UIRenderer.showReaderSelectFrame((ReaderSelectListener)AddLoanDialog.this);
					}
				});
				btnSelectReader.setBounds(458, 17, 109, 23);
				contentPanel.add(btnSelectReader);
			}
			{
				JButton btnRemoveBook = new JButton("Remove Book");
				btnRemoveBook.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (table.getSelectedRow()!= -1) {
							books.remove(table.getSelectedRow());
							returnDates.remove(table.getSelectedRow());
							onUpdate();
						}else {
							UIRenderer.showMessageDialog("Please select a record");
						}

					}
				});
				btnRemoveBook.setBounds(458, 95, 109, 23);
				contentPanel.add(btnRemoveBook);
			}
			{
				final String [] tableHeaders = {"Book Title", "Return Date"};
				model = new DefaultTableModel(){
					public int getRowCount() {
						return books.size();
						}
					
					public int getColumnCount() {
						return tableHeaders.length;
					};
					
					public String getColumnName(int col) {
						return tableHeaders[col];
					};
					
					public Object getValueAt(int row, int col) {
						switch (col){
						case 0 : 
							return books.get(row);							
							
						case 1 : 
							SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
							return df.format(returnDates.get(row));							
												
						default : 
							
							return"";
							
						}
					};
					
					public void setValueAt(Object value, int row, int col) {
						switch (col){
						case 0 : 
							books.set(row, (String) value);							
							
						case 1 : 
							try{
								String val = (String)value;
								String [] parts = val.split("-");
								Calendar cal = Calendar.getInstance();
								cal.set(Calendar.YEAR, Integer.parseInt(parts[0]));
								cal.set(Calendar.MONTH, Integer.parseInt(parts[1])-1);
								cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(parts[2]));
								Date date = cal.getTime();
								returnDates.set(row,date);		
							}catch(Exception ex){
								UIRenderer.showMessageDialog("Please Enter a valid date");
							}
												
						default : 
							
						}
					};
				};
				JPanel panel = new JPanel();
				panel.setBounds(120, 68, 309, 130);
				contentPanel.add(panel);
				table = new JTable(model);
				
				JTableHeader header = table.getTableHeader();
				panel.setLayout(new BorderLayout());
				panel.add(header, BorderLayout.NORTH);
				panel.add(table, BorderLayout.CENTER);
			}
			
			daysTxt = new JTextField();
			daysTxt.setBounds(322, 209, 107, 20);
			contentPanel.add(daysTxt);
			daysTxt.setColumns(10);
			
			JButton btnSetDate = new JButton("Set Date");
			btnSetDate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(daysTxt.getText() != "" && Integer.parseInt(daysTxt.getText()) > 0){
					if(table.getSelectedRow() != -1){
						int numofDays = Integer.parseInt(daysTxt.getText());
						int selectedRow = table.getSelectedRow(); 
						  Date date = returnDates.get(selectedRow); // your date
						    Calendar cal = Calendar.getInstance();
						    cal.setTime(date);
						   
						    int day = cal.get(Calendar.DAY_OF_MONTH);
						    cal.set(Calendar.DAY_OF_MONTH, day + numofDays);
						    
						Date newDate = cal.getTime();
						 returnDates.set(selectedRow, newDate);
						 onUpdate();
						 daysTxt.setText("");
					}
				}
				else{
					UIRenderer.showMessageDialog("Please Enter a valid number of days");
				}}
			});
			btnSetDate.setBounds(322, 240, 107, 23);
			contentPanel.add(btnSetDate);
			
			JLabel lblEnterLoanPeriod = new JLabel("Loan Period In Days");
			lblEnterLoanPeriod.setBounds(203, 212, 109, 14);
			contentPanel.add(lblEnterLoanPeriod);
			btnAddBook.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					UIRenderer.showSelectBookFrame(AddLoanDialog.this, ViewType.AVALIABLE);
				}
			});
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent event) {
						String reader = readerTxt.getText();
						
						addNewLoan(reader, AddLoanDialog.this.books);
						
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						UIRenderer.hideAddLoanDialog();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	/**
	 * Initialize properties.
	 */
	private void initialize(LoanUpdateActionListener event) {
		this.addEvent = event;
		this.books = new ArrayList<String>();
		this.returnDates = new ArrayList<Date>();
		
		// books.add("ShaminTitle001");
		// this.returnDates.add (new Date ());
	}

	protected void addNewLoan(String readerId, ArrayList<String> books) {
			ILoanService service = ServiceFactory.getLoanService();
			String id = service.addLoan(books, readerId, this.returnDates);
			
			if (!id.equalsIgnoreCase("")) {
				this.addEvent.onAdd();
				UIRenderer.hideAddLoanDialog();
			}
			
	}

	/**
	 * Clear all inputs.
	 */
	public void clearInputs() {
		this.readerTxt.setText(emptyString);
		
			
		this.books.clear();
	}

	@Override
	public void onUpdate() {
		this.model.fireTableDataChanged();
	}

	@Override
	public void onReaderSelect(String id) {
		this.readerTxt.setText(id);
		
	}

	@Override
	public void onDelete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBookSelect(String bookId) {
		this.books.add(bookId);
		this.returnDates.add(new Date());
		System.out.println("Book Select");
		this.model.fireTableDataChanged();
	}

	@Override
	public void onAdd() {
		// TODO Auto-generated method stub
		
	}
}
