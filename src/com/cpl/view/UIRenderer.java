package com.cpl.view;

import javax.swing.JDialog;
import javax.swing.JFrame;

public class UIRenderer {
	
	/**
	 * The main window.
	 */
	private static JFrame mainWindow;
	
	/**
	 * The login frame.
	 */
	private static JFrame loginFrame;
	
	/**
	 * The book management frame.
	 */
	private static JFrame bookMgtFrame;
	
	/**
	 * The book list view.
	 */
	private static BookFrame bookFrame;
	
	/**
	 * The reader list view.
	 */
	private static ReaderFrame readerFrame;
	
	/**
	 * The information message dialog.
	 */
	private static JDialog messageDialog;
	
	/**
	 * The dialog to add a new book.
	 */
	private static AddBookDialog addBookDialog;
	
	/**
	 * The dialog to add a new book.
	 */
	private static UpdateBookDialog updateBookDialog;
	
	/**
	 * The dialog to add new readers.
	 */
	private static AddReaderDialog addReaderDialog;
	
	/**
	 * The dialog to update Readers.
	 */
	private static UpdateReaderDialog updateReaderDialog;
	
	/**
	 * The add loan dialog.
	 */
	private static AddLoanDialog addLoanDialog;
	
	/**
	 * The update loan dialog.
	 */
	private static UpdateLoanDialog updateLoanDialog;
	
	/**
	 * The loan frame.
	 */
	private static LoanFrame loanFrame;
	
	/**
	 * The book Select frame.
	 */
	private static BookSelectFrame bookSelectFrame;
	
	/**
	 * The reader select frame.
	 */
	private static ReaderSelectFrame readerSelectFrame;
	
	/**
	 * The return's record manage view.
	 */
	private static ReturnFrame returnFrame;
	
	/**
	 * The add new return record view.
	 */
	private static AddReturnFrame addReturnFrame;
	
	/**
	 * show the login frame.
	 */
	public static void showLoginFrame()
	{		
		loginFrame = new JFrame();
		loginFrame.setContentPane(new LoginPanel());
		loginFrame.setBounds(100, 100, 450, 300);
		loginFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		loginFrame.setVisible(true);
	}
	
	/**
	 * Hide the login frame.
	 */
	public static void hideLoginFrame() {
		if (UIRenderer.loginFrame != null) {
			loginFrame.setVisible(false);
		}
	}
	
	/**
	 * Show the book management frame.
	 */
	public static void showBookManagementFrame() {
		if (UIRenderer.bookMgtFrame == null) {
			bookMgtFrame = new LibraryMgtFrame();
			bookMgtFrame.setBounds(100, 100, 450, 300);
			bookMgtFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		}		
		bookMgtFrame.setVisible(true);
	}
	
	/**
	 * Hide book management frame.
	 */
	public static void hideBookManagementFrame() {
		if (UIRenderer.bookMgtFrame != null) {
			UIRenderer.bookMgtFrame.setVisible(false);
		}
	}
	
	/**
	 * show the main application window.
	 */
	public static void showMainWindow()	{
		if(UIRenderer.mainWindow == null){
			mainWindow = new MainWindow();
			mainWindow.setBounds(100, 100, 450, 300);
			mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		mainWindow.setVisible(true);
	}
	
	/**
	 * @param status the level of error
	 */
	public static void exitApplication(int status){
		System.exit(status);
	}
	
	/**
	 * @param message the information to be delivered to user.
	 */
	public static void showMessageDialog(String message) {
		if (UIRenderer.messageDialog == null) {
			messageDialog = new MessageDialog(message);
		}
		messageDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		messageDialog.setVisible(true);
	}
	
	/**
	 * Show the add book dialog.
	 * @param event Book Update action listener event.
	 */
	public static void showAddBookDialog(BookUpdateActionListener event) {
		if (UIRenderer.addBookDialog == null) {
			addBookDialog = new AddBookDialog(event);
		}
		addBookDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		addBookDialog.setVisible(true);
	}

	/**
	 * Hide the add book dialog.
	 */
	public static void hideAddBookDialog() {
		if (UIRenderer.addBookDialog != null) {
			((AddBookDialog)addBookDialog).clearInputs();
			addBookDialog.dispose();
		}
	}
	
	/**
	 * Show the add book dialog.
	 * @param id Identifier of the book to be updated.
	 * @param event Book update action Event.
	 */
	public static void showUpdateBookDialog(int id, BookUpdateActionListener event) {
		if (UIRenderer.updateBookDialog == null) {
			updateBookDialog = new UpdateBookDialog(event);
		}
		updateBookDialog.initialize(id);
		updateBookDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		updateBookDialog.setVisible(true);
	}

	/**
	 * Hide the book update dialog.
	 */
	public static void hideUpdateBookDialog() {
		if (UIRenderer.updateBookDialog != null) {
			updateBookDialog.clearInputs();
			updateBookDialog.dispose();
		}
		
	}

	/**
	 * show book frame.
	 */
	public static void showBookFrame() {
		if(UIRenderer.bookFrame == null){
			bookFrame = new BookFrame();	
			bookFrame.setBounds(100, 100, 450, 300);
			bookFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		}	
		
		bookFrame.setVisible(true);
	}
	
	/**
	 * show book frame.
	 */
	public static void showReaderFrame() {
		if(UIRenderer.readerFrame == null){
			readerFrame = new ReaderFrame();	
			readerFrame.setBounds(100, 100, 450, 300);
			readerFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		}	
		
		readerFrame.setVisible(true);
	}
	
	/**
	 * Show the add reader dialog.
	 * @param event reader Update action listener event.
	 */
	public static void showAddReaderDialog(ReaderUpdateActionListener event) {
		if (UIRenderer.addReaderDialog == null) {
			addReaderDialog = new AddReaderDialog(event);
		}
		addReaderDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		addReaderDialog.setVisible(true);
	}

	/**
	 * Hide the add reader dialog.
	 */
	public static void hideAddReaderDialog() {
		if (UIRenderer.addReaderDialog != null) {
			addReaderDialog.clearInputs();
			addReaderDialog.dispose();
		}
		
	}

	/**
	 * @param selected Selected row of the reader table
	 * @param event Action listenr for update event.
	 */
	public static void showUpdateReaderDialog(int selected, ReaderUpdateActionListener event) {
		if (UIRenderer.updateReaderDialog == null) {
			updateReaderDialog = new UpdateReaderDialog(event);
		}
		
		updateReaderDialog.initialize(selected);
		updateReaderDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		updateReaderDialog.setVisible(true);
		
	}
	
	/**
	 * Hide reader update dialog.
	 */
	public static void hideUpdateReaderDialog() {
		if (UIRenderer.updateReaderDialog != null) {
			updateReaderDialog.clearInputs();
			updateReaderDialog.dispose();
		}
		
	}
	
	/**
	 * Show loan frame.
	 */
	public static void showLoanFrame() {
		if(UIRenderer.loanFrame == null){
			loanFrame = new LoanFrame();	
			loanFrame.setBounds(100, 100, 450, 300);
			loanFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		}	
		
		loanFrame.setVisible(true);
	}

	public static void showAddLoanDialog(LoanUpdateActionListener event) {
		if (UIRenderer.addLoanDialog == null) {
			addLoanDialog = new AddLoanDialog(event);
		}
		
		addLoanDialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addLoanDialog.setVisible(true);
	}

	public static void showUpdateLoanDialog(int selected,
			LoanUpdateActionListener event) {
		if (UIRenderer.updateLoanDialog == null) {
			updateLoanDialog = new UpdateLoanDialog(event);
		}
		
		updateReaderDialog.initialize(selected);
		updateReaderDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		updateReaderDialog.setVisible(true);
	}
	
	/**
	 * Hide update loan dialog.
	 */
	public static void hideUpdateLoanDialog() {
		if (UIRenderer.updateLoanDialog != null) {
			updateLoanDialog.clearInputs();
			updateLoanDialog.dispose();
		}
		
	}
	
	/**
	 * Hide add loan dialog.
	 */
	public static void hideAddLoanDialog() {
		if (UIRenderer.addLoanDialog != null) {
			addLoanDialog.clearInputs();
			addLoanDialog.dispose();
		}
		
	}

	public static void showSelectBookFrame(BookSelectListener event, ViewType type) {
		if (UIRenderer.bookSelectFrame == null) {
			UIRenderer.bookSelectFrame = new BookSelectFrame(type);			
		}
		
		bookSelectFrame.initialize(event);
		bookSelectFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		bookSelectFrame.setVisible(true);
		
	}
	
	public static void hideBookSelectFrame() {
		if (UIRenderer.bookSelectFrame != null) {
			bookSelectFrame.clearInputs();
			bookSelectFrame.dispose();
			bookSelectFrame = null;
		}
	}

	public static void showReaderSelectFrame(
			ReaderSelectListener event) {
		if (UIRenderer.readerSelectFrame == null) {
			UIRenderer.readerSelectFrame = new ReaderSelectFrame();			
		}
		
		readerSelectFrame.initialize(event);
		readerSelectFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		readerSelectFrame.setVisible(true);
		
	}

	public static void hideReaderSelectFrame() {
		if (UIRenderer.readerSelectFrame != null) {
			readerSelectFrame.clearInputs();
			readerSelectFrame.dispose();
		}
		
	}

	public static void showAddReturnDialog(ReturnActionListener event) {
		if (UIRenderer.addReturnFrame == null) {
			UIRenderer.addReturnFrame = new AddReturnFrame(event);			
		}
		
		addReturnFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addReturnFrame.setVisible(true);
		
		
	}

	public static void showReturnFrame() {
		if (UIRenderer.returnFrame == null) {
			UIRenderer.returnFrame = new ReturnFrame();			
		}
		
		returnFrame.initialize();
		returnFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		returnFrame.setVisible(true);
		
		
	}

	public static void hideAddReturnDialog() {
		if (UIRenderer.addReturnFrame != null) {
			addReturnFrame.dispose();
			addReturnFrame = null;
		}
		
	}
}
