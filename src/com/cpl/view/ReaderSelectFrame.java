package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This will list all the book to a data grid.
 */
public class ReaderSelectFrame extends JFrame {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 4780997252713603189L;
	
	/**
	 * The content pane.
	 */
	private JPanel contentPane;
	private JPanel panel;
	private JButton btnSelectBook;
	private JTable readerTable;
	private ReaderTableModel model;
	private ReaderSelectListener updateEvent;
	/**
	 * Create the frame.
	 */
	public ReaderSelectFrame() {
		setTitle("Select Reader");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 1200, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		model = new ReaderTableModel();
		readerTable = new JTable(model);
		
		JScrollPane scrollPane = new JScrollPane(readerTable);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.EAST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		btnSelectBook = new JButton("Select");
		btnSelectBook.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int selected = readerTable.getSelectedRow();
				String title = (String) ReaderSelectFrame.this.model.getValueAt(selected, 0);
				ReaderSelectFrame.this.updateEvent.onReaderSelect(title);	
				UIRenderer.hideReaderSelectFrame();
			}
		});
		btnSelectBook.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnSelectBook);
		
	}
	
	/**
	 * initialize the frame.
	 */
	public void initialize(ReaderSelectListener updateEvent){
		this.updateEvent = updateEvent;
	}

	public void clearInputs() {
				
	}
	
}
