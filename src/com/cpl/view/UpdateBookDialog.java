package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.cpl.data.Book;
import com.cpl.data.SampleData;
import com.cpl.service.BookMgtService;
import com.cpl.service.IBookMgtService;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This dialog will add a new book to the library.
 */
public class UpdateBookDialog extends JDialog {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 2309200214777748119L;
	
	/**
	 * The empty string.
	 */
	private static final String emptyString = "";
	
	
	/**
	 * The content panel.
	 */
	private final JPanel contentPanel = new JPanel();
	
	/**
	 * The title text field;
	 */
	private JTextField titleTextField;
	
	/**
	 * The author text field.
	 */
	private JTextField authorTextField;
	
	
	/**
	 * The publisher text field.
	 */
	private JTextField publisherTextField;
	
	/**
	 * The id of the book.
	 */
	private String bookId;
	
	/**
	 * The update event.
	 */
	private BookUpdateActionListener updateEvent;
	
	/**
	 * @return the id of the book
	 */
	public String getBookId() {
		return bookId;
	}

	/**
	 * @param bookId set book id.
	 */
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	
	/**
	 * @param rowId set it by row
	 */
	public void setBookId(int rowId) {
		Book book = SampleData.getInstance().getBooks().get(rowId);
		this.bookId = book.getId();
	}

	/**
	 * Create the dialog.
	 * @param event Book update action listener event. 
	 */
	public UpdateBookDialog(BookUpdateActionListener event) {
		this.updateEvent = event;
		setTitle("Update book");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblTitle = new JLabel("Title");
			lblTitle.setBounds(76, 17, 86, 21);
			contentPanel.add(lblTitle);
		}
		{
			titleTextField = new JTextField();
			titleTextField.setBounds(187, 9, 144, 36);
			contentPanel.add(titleTextField);
			titleTextField.setColumns(10);
		}
		{
			JLabel lblAuthor = new JLabel("Author");
			lblAuthor.setBounds(76, 68, 86, 21);
			contentPanel.add(lblAuthor);
		}
		
		authorTextField = new JTextField();
		authorTextField.setBounds(187, 60, 144, 36);
		contentPanel.add(authorTextField);
		authorTextField.setColumns(10);
		
		JLabel lblPublisher = new JLabel("Publisher");
		lblPublisher.setBounds(76, 121, 86, 21);
		contentPanel.add(lblPublisher);
		
		publisherTextField = new JTextField();
		publisherTextField.setBounds(187, 113, 144, 36);
		contentPanel.add(publisherTextField);
		publisherTextField.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent event) {
						String title = titleTextField.getText();
						String author = authorTextField.getText();
						String publisher = publisherTextField.getText();
						
						updateBook(title, author, publisher);
						
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						UIRenderer.hideUpdateBookDialog();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	/**
	 * Initialize view with given book data.
	 * @param rowId Row identifier of the book list.
	 */
	public void initialize(int rowId) {
		setBookId(rowId);
		IBookMgtService service = new BookMgtService();
		Book book = service.getById(getBookId());
		if (book != null) {
			this.titleTextField.setText(book.getTitle());
			this.authorTextField.setText(book.getAuthor());
			this.publisherTextField.setText(book.getPublisher());
		}else{
			UIRenderer.showMessageDialog("No book for given id");
		}
		
	}

	/**
	 * @param title of the book.
	 * @param author of the book.
	 * @param publisher of the book.
	 */
	protected void updateBook(String title, String author, String publisher) {
			IBookMgtService service = new BookMgtService();
			
			Book book = new Book();
			book.setId(bookId);
			book.setTitle(title);
			book.setAuthor(author);
			book.setPublisher(publisher);
			
			boolean success = service.updateBook(book);
			
			if (success) {
				UIRenderer.hideUpdateBookDialog();
				updateEvent.onUpdate();
			}
	}

	/**
	 * Clear all inputs.
	 */
	public void clearInputs() {
		this.titleTextField.setText(emptyString);
		this.authorTextField.setText(emptyString);
		this.publisherTextField.setText(emptyString);
		
	}
	
	
}
