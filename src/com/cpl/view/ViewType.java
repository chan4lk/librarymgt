package com.cpl.view;

public enum ViewType {
	ALL(1),
	AVALIABLE(2),
	ON_LOAN(3),
	BY_TITLE(4);
	
	private int value;
	
	private ViewType(int value){
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}
}
