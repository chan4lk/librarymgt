package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import com.cpl.service.IReaderService;
import com.cpl.service.ServiceFactory;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This will list all the book to a data grid.
 */
public class ReaderFrame extends JFrame implements ReaderUpdateActionListener {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 4780297252713603189L;
	
	/**
	 * The content pane.
	 */
	private JPanel contentPane;
	private JTable readerTable;
	private JPanel panel;
	private JButton btnAddReader;
	private JButton btnUpdate;
	private JButton btnDelete;
	private ReaderTableModel model;

	/**
	 * Create the frame.
	 */
	public ReaderFrame() {
		setTitle("Manage Readers");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 1200, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		model = new ReaderTableModel();
		readerTable = new JTable(model);
		
		JScrollPane scrollPane = new JScrollPane(readerTable);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.EAST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		btnAddReader = new JButton("Add Reader");
		btnAddReader.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				UIRenderer.showAddReaderDialog(ReaderFrame.this);				
			}
		});
		btnAddReader.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnAddReader);
		
		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				int selected = readerTable.getSelectedRow();
				if (selected == -1) {
					UIRenderer.showMessageDialog("Please Select a record");
				}else{
					UIRenderer.showUpdateReaderDialog(selected, (ReaderUpdateActionListener)ReaderFrame.this);
				}
			}
		});
		btnUpdate.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnUpdate);
		
		btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int selected = readerTable.getSelectedRow();
				IReaderService service = ServiceFactory.getReaderService();
				boolean success = service.deleteReader(selected);
				if (success) {
					ReaderFrame.this.onDelete();
					service.listReaders();
				}
			}
		});
		btnDelete.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnDelete);
		
	}
	
	/**
	 * initialize the frame.
	 */
	public void initialize(){
		
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onUpdate()
	 */
	@Override
	public void onUpdate() {
		System.out.println("On update Reader Frame.");
		model.fireTableDataChanged();		
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onAdd()
	 */
	@Override
	public void onAdd() {
		System.out.println("On Add Reader Frame.");
		model.fireTableDataChanged();		
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onDelete()
	 */
	@Override
	public void onDelete() {
		System.out.println("On Delete Reader Frame.");
		model.fireTableDataChanged();		
	}

}
