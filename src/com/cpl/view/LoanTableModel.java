package com.cpl.view;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.cpl.data.Book;
import com.cpl.data.Loan;
import com.cpl.service.IBookMgtService;
import com.cpl.service.ILoanService;
import com.cpl.service.ServiceFactory;

public class LoanTableModel extends AbstractTableModel{
	 
	/**
	 * The generated serial id.
	 */
	private static final long serialVersionUID = -8691249976928976528L;
	
	/**
	 * the books.
	 */
	private ArrayList<Loan> loans;
	
	/**
	 * The headers.
	 */
	private String headerList[];
	
	/**
	 * The loan service.
	 */
	private ILoanService service;
	
	/**
	 * The book service
	 */
	private IBookMgtService bookService;
	 
	/**
	 * Create a new TableModel.
	 */
	public LoanTableModel() {
		this.service = ServiceFactory.getLoanService();		
		this.bookService = ServiceFactory.getBookMgtService();
		this.loans = this.service.getActiveLoans();
		this.headerList = this.service.getColumnNames();
	}
	 
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return headerList.length;
	}
	 
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
	return loans.size();
	}
	 
	/*
	 * this method is called to set the value of each cell
	 */
	@Override
	public Object getValueAt(int row, int column) {
		Loan entity = null;
		entity= loans.get(row);
	 
		switch (column) {
		 
			case 0:
				return entity.getId();
			case 1:
				return entity.getBook();
			case 2:
				return entity.getLoanDate();
			case 3:
				return entity.getReturnDate();
			case 4:
				return entity.getReaderId();
					 
			default :
		}
		 
		return "";
	}
	 
	 
	

	//This method will be used to display the name of columns
	public String getColumnName(int col) {
		return headerList[col];
	}
}
