package com.cpl.view;

/**
 * @author Chandima.Ranaweera
 *
 */
public interface LoanUpdateActionListener {
	/**
	 * On loan update this event will be fired.
	 */
	void onUpdate();
	
	/**
	 * on Add event.
	 */
	void onAdd();
	

	
	/**
	 * When a loan is deleted this event will be fired.
	 */
	void onDelete();

}
