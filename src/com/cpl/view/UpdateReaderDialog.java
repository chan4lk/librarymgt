package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.cpl.data.Reader;
import com.cpl.service.IReaderService;
import com.cpl.service.ServiceFactory;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This dialog will add a new book to the library.
 */
public class UpdateReaderDialog extends JDialog {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 2309200214777748119L;
	
	/**
	 * The empty string.
	 */
	private static final String emptyString = "";
	
	
	/**
	 * The content panel.
	 */
	private final JPanel contentPanel = new JPanel();
	
	/**
	 * The title text field;
	 */
	private JTextField fNameTxt;
	
	/**
	 * The author text field.
	 */
	private JTextField lNameTxt;
	
	/**
	 * The id of the book.
	 */
	private String readerId;
	
	/**
	 * The update event.
	 */
	private ReaderUpdateActionListener updateEvent;
	
	/**
	 * The reader service.
	 */
	private IReaderService readerService = ServiceFactory.getReaderService();
	
	/**
	 * @param rowId set it by row
	 */
	public void setReaderId(int rowId) {
		Reader reader = readerService.getById(rowId);
		this.readerId = reader.getId();
	}

	/**
	 * Create the dialog.
	 * @param event Book update action listener event. 
	 */
	public UpdateReaderDialog(ReaderUpdateActionListener event) {
		this.updateEvent = event;
		setTitle("Update Reader information");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblFirstName = new JLabel("First Name");
			lblFirstName.setBounds(77, 64, 86, 21);
			contentPanel.add(lblFirstName);
		}
		{
			fNameTxt = new JTextField();
			fNameTxt.setBounds(188, 56, 144, 36);
			contentPanel.add(fNameTxt);
			fNameTxt.setColumns(10);
		}
		{
			JLabel lblLastName = new JLabel("Last Name");
			lblLastName.setBounds(77, 115, 86, 21);
			contentPanel.add(lblLastName);
		}
		
		lNameTxt = new JTextField();
		lNameTxt.setBounds(188, 107, 144, 36);
		contentPanel.add(lNameTxt);
		lNameTxt.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent event) {
						String firstName = fNameTxt.getText();
						String lastName = lNameTxt.getText();
						
						updateReader(firstName, lastName);
						
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						UIRenderer.hideUpdateReaderDialog();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	/**
	 * Initialize view with given book data.
	 * @param rowId Row identifier of the book list.
	 */
	public void initialize(int rowId) {
		setReaderId(rowId);
		Reader reader = readerService.getById(rowId);
		
		if (reader != null) {
			this.fNameTxt.setText(reader.getFirstName());
			this.lNameTxt.setText(reader.getLastName());
		}else{
			UIRenderer.showMessageDialog("No Reader for given id");
		}
		
	}

	/**
	 * @param firstName First Name of the reader.
	 * @param lastName Last Name of the reader.
	 */
	protected void updateReader(String firstName, String lastName) {
			IReaderService service = ServiceFactory.getReaderService();
			
			Reader reader = new Reader();
			reader.setId(this.readerId);
			reader.setFirstName(firstName);
			reader.setLastName(lastName);
			
			boolean success = service.updateReader(reader);
			
			if (success) {
				UIRenderer.hideUpdateReaderDialog();
				updateEvent.onUpdate();
			}
	}

	/**
	 * Clear all inputs.
	 */
	public void clearInputs() {
		this.fNameTxt.setText(emptyString);
		this.lNameTxt.setText(emptyString);
		
	}
	
	
}
