package com.cpl.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.cpl.service.ISecurityService;
import com.cpl.service.SecurityService;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 */
public class LoginPanel extends JPanel {
	/**
	 * The default serial version id;
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The user name text field.
	 */
	private JTextField userNameTextField;
	
	/**
	 * The password field.
	 */
	private JPasswordField passwordField;
	
	/**
	 * Create the panel.
	 */
	public LoginPanel() {
		setLayout(null);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setLocation(61, 75);
		add(lblUsername);
		lblUsername.setSize(82, 24);
		
		userNameTextField = new JTextField();
		userNameTextField.setBounds(165, 66, 191, 33);
		add(userNameTextField);
		userNameTextField.setColumns(15);
		
		JLabel lblpassword = new JLabel("Password");
		lblpassword.setBounds(61, 134, 82, 24);
		add(lblpassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(165, 125, 191, 33);
		add(passwordField);
		passwordField.setColumns(15);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setBounds(165, 200, 191, 33);
		btnLogin.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent event) {
				ISecurityService service = new SecurityService();
				boolean success = service.SignIn(userNameTextField.getText(), passwordField.getText());
				if (success) {
					showBookManagement();
					hideMe();
				}else{
					showErrorMessage();
				}
			}
		});
		
		add(btnLogin);

	}
	
	/**
	 * hide the login frame.
	 */
	protected void hideMe() {
		UIRenderer.hideLoginFrame();
	}

	/**
	 * Show login error.
	 */
	protected void showErrorMessage() {		
		JOptionPane.showMessageDialog(this,
                "Invalid username or password. Try again.",
                "Error Message",
                JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Show book management window.
	 */
	protected void showBookManagement()
	{
		UIRenderer.showBookManagementFrame();		
	}
}
