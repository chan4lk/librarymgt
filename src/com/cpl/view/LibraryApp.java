package com.cpl.view;

import java.awt.EventQueue;

public class LibraryApp {

	
	/**
	 * @param args arguments list from command line.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LibraryApp window = new LibraryApp();
					window.initialize();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LibraryApp() {
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		UIRenderer.showMainWindow();
	}

}
