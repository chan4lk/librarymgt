package com.cpl.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.cpl.service.BookMgtService;
import com.cpl.service.IBookMgtService;

/**
 * @author Chandima.Ranaweera
 *
 */
public class LibraryMgtFrame extends JFrame implements BookUpdateActionListener{

	/**
	 * The default serial version id.
	 */
	private static final long serialVersionUID = 1L;
	
	
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public LibraryMgtFrame() {
		setTitle("Colombo Public Library");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 100, 652, 402);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(2, 2, 0, 0));
		
		JButton btnLoan = new JButton("Loans");
		btnLoan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				UIRenderer.showLoanFrame();
			}
		});
		contentPane.add(btnLoan);
		
		JButton btnReader = new JButton("Readers");
		btnReader.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				UIRenderer.showReaderFrame();
			}
		});
		contentPane.add(btnReader);
		
		JButton btnReturn = new JButton("Returns");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UIRenderer.showReturnFrame();
			}
		});
		contentPane.add(btnReturn);
		
		JButton btnBook = new JButton("Books");
		btnBook.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UIRenderer.showBookFrame();
				IBookMgtService service = new BookMgtService();
				service.listBooks();
			}
		});
		contentPane.add(btnBook);
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onUpdate()
	 */
	@Override
	public void onUpdate() {
		System.out.println("book updated.");
		
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onAdd()
	 */
	@Override
	public void onAdd() {
		System.out.println("book added.");
		
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onDelete()
	 */
	@Override
	public void onDelete() {
		System.out.println("book delete.");
		
	}

	@Override
	public void onSelect(String id) {
		// TODO Auto-generated method stub
		
	}

}
