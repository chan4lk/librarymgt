package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import com.cpl.service.IReturnService;
import com.cpl.service.ServiceFactory;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This will list all the book to a data grid.
 */
public class ReturnFrame extends JFrame implements ReturnActionListener {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 4780997252713603189L;
	
	/**
	 * The content pane.
	 */
	private JPanel contentPane;
	private JTable dataTable;
	private JPanel panel;
	private JButton btnAddReturn;
	private JButton btnDelete;
	private ReturnTableModel model;

	private IReturnService service;
	/**
	 * Create the frame.
	 */
	public ReturnFrame() {
		setTitle("Manage Returns");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 1200, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		model = new ReturnTableModel();
		dataTable = new JTable(model);
		
		JScrollPane scrollPane = new JScrollPane(dataTable);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.EAST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		btnAddReturn = new JButton("Add Return");
		btnAddReturn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				UIRenderer.showAddReturnDialog(ReturnFrame.this);				
			}
		});
		btnAddReturn.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnAddReturn);
		
		btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int selected = dataTable.getSelectedRow();
				if (selected == -1) {
					UIRenderer.showMessageDialog("Please selected a record");
				}else{
					
					boolean success = service.deleteReturn(selected);
					if (success) {
						ReturnFrame.this.onUpdate();
						service.listReturns();
					}
				}
			}
		});
		btnDelete.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnDelete);
		
	}
	
	/**
	 * initialize the frame.
	 */
	public void initialize(){
		service = ServiceFactory.getReturnService();
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onUpdate()
	 */
	@Override
	public void onUpdate() {
		System.out.println("On update Return Frame.");
		service.listReturns();
		model.fireTableDataChanged();		
	}


}
