package com.cpl.view;

public interface ReaderSelectListener {
	/**
	 * When new loan is added this event will be fired.
	 */
	void onReaderSelect(String title);
}
