package com.cpl.view;

import com.cpl.data.BookStatus;

public class BookInfo {
	private String bookId;
	
	private BookStatus status;

	public BookInfo(String bookId, BookStatus status) {
		this.bookId = bookId;
		this.status = status;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public BookStatus getStatus() {
		return status;
	}

	public void setStatus(BookStatus status) {
		this.status = status;
	}
	
	
}
