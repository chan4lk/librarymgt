package com.cpl.view;

public interface BookSelectListener {
	void onBookSelect(String id);
}
