package com.cpl.view;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.cpl.data.Book;
import com.cpl.service.IBookMgtService;
import com.cpl.service.ServiceFactory;

public class BookTableModel extends AbstractTableModel{
	 
	/**
	 * The generated serial id.
	 */
	private static final long serialVersionUID = -8691249976928976528L;
	
	/**
	 * the books.
	 */
	private ArrayList<Book> books;
	
	/**
	 * The headers.
	 */
	private String headerList[];
	
	/**
	 * The book service.
	 */
	private IBookMgtService service;
	 
	/**
	 * Create a new TableModel.
	 */
	public BookTableModel(ViewType type) {
		this.service = ServiceFactory.getBookMgtService();		
		switch (type) {
		case ALL:
			books = this.service.getActiveBooks();
			break;
		case AVALIABLE:
			books = this.service.getAvailableBooks();
			break;
		case ON_LOAN:
			books = this.service.getBooksOnLoan();
			break;
		case BY_TITLE:
			books = this.service.getByTitlte();
		default:
			books = this.service.getActiveBooks();
			break;
		}
		
		headerList = this.service.getBookColums();
	}
	 
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return headerList.length;
	}
	 
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
	return books.size();
	}
	 
	/*
	 * this method is called to set the value of each cell
	 */
	@Override
	public Object getValueAt(int row, int column) {
		Book entity = null;
		entity= books.get(row);
	 
		switch (column) {
			case 0:
				return entity.getId();
			case 1:
				return entity.getTitle();
			case 2:
				return entity.getAuthor();
			case 3:
				return entity.getPublisher();
			case 4:
				return entity.isReserved();
			case 5:
				return entity.isBorrowed();
			case 6:
				return entity.isDeleted();
					 
			default :
		}
		 
		return "";
	}
	 
	 
	 //This method will be used to display the name of columns
	public String getColumnName(int col) {
		return headerList[col];
	}
}
