package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.cpl.data.BookStatus;
import com.cpl.service.IReturnService;
import com.cpl.service.ServiceFactory;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This dialog will add a new book to the library.
 */
public class AddReturnFrame extends JFrame implements ReturnActionListener, ReaderSelectListener, BookSelectListener {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 2309200214777748119L;
	
	/**
	 * The empty string.
	 */
	private static final String emptyString = "";
	
	
	/**
	 * The content panel.
	 */
	private final JPanel contentPanel = new JPanel();
	
	/**
	 * The title text field;
	 */
	private JTextField readerTxt;
	
	/**
	 * Books List.
	 */
	private ArrayList<String> books;
	
	/**
	 * Book and status.
	 */
	private ArrayList<BookInfo> bookStatus;
	
	/**
	 * Event to fire when book is added.
	 */
	private ReturnActionListener addEvent;

	/**
	 * The selected book.
	 */
	private String selectedBook;
	
	/**
	 * The book list;
	 */
	private SwingJList<String> list;
	

	/**
	 * Create the dialog.
	 * @param event This event fires when book is added.
	 */
	public AddReturnFrame(ReturnActionListener event) {
		this.initialize(event);
		
		setTitle("Add new Return");
		setBounds(100, 100, 644, 362);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblReader = new JLabel("Reader ID");
			lblReader.setBounds(76, 17, 86, 21);
			contentPanel.add(lblReader);
		}
		{
			readerTxt = new JTextField();
			readerTxt.setBounds(187, 9, 144, 36);
			contentPanel.add(readerTxt);
			readerTxt.setColumns(10);
		}
		{
			JLabel lblBooks = new JLabel("Books");
			lblBooks.setBounds(76, 68, 86, 21);
			contentPanel.add(lblBooks);
		}
		
		JButton btnAddBook = new JButton("Add Book");
		
		btnAddBook.setBounds(341, 67, 109, 23);
		contentPanel.add(btnAddBook);
		{
			list = new SwingJList<String>(this.books);
			
			list.setBounds(188, 68, 143, 149);
			list.addListSelectionListener(new ListSelectionListener() {
				
				@Override
				public void valueChanged(ListSelectionEvent event) {
					 if (!event.getValueIsAdjusting()) {
						 selectedBook = (String) list.getSelectedValue();
					 }
					
				}
			});
			contentPanel.add(list);
			{
				JButton btnSelectReader = new JButton("Select Reader");
				btnSelectReader.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						UIRenderer.showReaderSelectFrame((ReaderSelectListener)AddReturnFrame.this);
					}
				});
				btnSelectReader.setBounds(341, 16, 109, 23);
				contentPanel.add(btnSelectReader);
			}
			{
				JButton btnRemoveBook = new JButton("Remove Book");
				btnRemoveBook.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (selectedBook != null && !selectedBook.equalsIgnoreCase("")) {
							list.removeElement(selectedBook);
						}else {
							UIRenderer.showMessageDialog("Please select a record");
						}

					}
				});
				btnRemoveBook.setBounds(341, 94, 109, 23);
				contentPanel.add(btnRemoveBook);
			}
			
			JCheckBox chckbxDamaged = new JCheckBox("Damaged");
			chckbxDamaged.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					for (BookInfo info : bookStatus) {
						if (info.getBookId() == selectedBook) {
							bookStatus.remove(info);
						}
					}
					bookStatus.add(new BookInfo(selectedBook, BookStatus.DAMAGED));
				}
			});
			chckbxDamaged.setBounds(341, 136, 97, 23);
			contentPanel.add(chckbxDamaged);
			btnAddBook.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event) {
					UIRenderer.showSelectBookFrame(
							(BookSelectListener) AddReturnFrame.this,
							ViewType.ON_LOAN);
				}
			});
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent event) {
						String reader = readerTxt.getText();
						
						addNewReturn(reader, AddReturnFrame.this.bookStatus);
						
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						UIRenderer.hideAddReturnDialog();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	/**
	 * Initialize properties.
	 */
	private void initialize(ReturnActionListener event) {
		this.addEvent = event;
		this.books = new ArrayList<String>();
		this.bookStatus = new ArrayList<BookInfo>();
		
	}

	protected void addNewReturn(String readerId,  ArrayList<BookInfo> books) {
			IReturnService service = ServiceFactory.getReturnService();
			for (BookInfo bookInfo : books) {
				service.addReturn(
						readerId, 
						bookInfo.getBookId(),
						bookInfo.getStatus(), 
						getFine(bookInfo.getStatus()));
				this.addEvent.onUpdate();
			}
					
			
			service.listReturns();
			UIRenderer.hideAddReturnDialog();
			
	}

	private double getFine(BookStatus status) {
		double amount = 0;
		switch (status) {
		case DAMAGED:
			amount = 100;
			break;
		default:
			amount = 0;
			break;
		}
		return amount;
	}

	/**
	 * Clear all inputs.
	 */
	public void clearInputs() {
		this.readerTxt.setText(emptyString);
		for (String book : books) {
			this.list.removeElement(book);
		}
		
		this.books.clear();
		this.bookStatus.clear();
	}

	@Override
	public void onUpdate() {
		System.out.println("On update at addreturnframe ");
		
	}

	@Override
	public void onReaderSelect(String id) {
		this.readerTxt.setText(id);
		
	}

	@Override
	public void onBookSelect(String bookId) {
		this.list.addElement(bookId);
		this.books.add(bookId);
		this.bookStatus.add(new BookInfo(bookId, BookStatus.DEFAULT));
	}
}
