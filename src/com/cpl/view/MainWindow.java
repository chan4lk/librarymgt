package com.cpl.view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * @author Chandima.Ranaweera
 *
 */
public class MainWindow extends JFrame {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 5158378658164011998L;
	
	/**
	 * The content pane.
	 */
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 254);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setSize(new Dimension(400, 100));
		btnLogin.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				UIRenderer.showLoginFrame();
				
			}
		});
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		contentPane.add(btnLogin);
		
		JButton btnExit = new JButton("Exit");
		btnExit.setSize(new Dimension(300, 100));
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				exitApplication();
			}
		});
		contentPane.add(btnExit);
	}

	/**
	 * Exit the application
	 */
	protected void exitApplication() {
		UIRenderer.exitApplication(0);
		
	}

}
