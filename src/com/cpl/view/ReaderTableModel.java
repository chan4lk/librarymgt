package com.cpl.view;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.cpl.data.Reader;
import com.cpl.service.IReaderService;
import com.cpl.service.ServiceFactory;

public class ReaderTableModel extends AbstractTableModel{
	 
	/**
	 * The generated serial id.
	 */
	private static final long serialVersionUID = -6233433915590032641L;

	/**
	 * the books.
	 */
	private ArrayList<Reader> readers;
	
	/**
	 * The headers.
	 */
	private String headerList[];
	
	/**
	 * The book service.
	 */
	private IReaderService service;
	 
	/**
	 * Create a new TableModel.
	 */
	public ReaderTableModel() {
		this.service = ServiceFactory.getReaderService();		
		readers = this.service.getActiveReaders();
		headerList = this.service.getHeaderColumns();
	}
	 
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return headerList.length;
	}
	 
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return readers.size();
	}
	 
	/*
	 * this method is called to set the value of each cell
	 */
	@Override
	public Object getValueAt(int row, int column) {
		Reader entity = null;
		entity= readers.get(row);
	 
		switch (column) {
			case 0:
				return entity.getId();
			case 1:
				return entity.getFirstName();
			case 2:
				return entity.getLastName();
			default :
		}
		 
		return "";
	}
	 
	 
	 //This method will be used to display the name of columns
	public String getColumnName(int col) {
		return headerList[col];
	}
}
