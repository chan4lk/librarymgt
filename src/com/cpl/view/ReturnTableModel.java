package com.cpl.view;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.cpl.data.Return;
import com.cpl.service.IReturnService;
import com.cpl.service.ServiceFactory;

public class ReturnTableModel extends AbstractTableModel{
	 
	/**
	 * The generated serial id.
	 */
	private static final long serialVersionUID = -6233433915590032641L;

	/**
	 * the books.
	 */
	private ArrayList<Return> returns;
	
	/**
	 * The headers.
	 */
	private String headerList[];
	
	/**
	 * The book service.
	 */
	private IReturnService service;
	 
	/**
	 * Create a new TableModel.
	 */
	public ReturnTableModel() {
		this.service = ServiceFactory.getReturnService();
		returns = this.service.getActiveReturns();
		headerList = this.service.getHeaderColumns();
	}
	 
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return headerList.length;
	}
	 
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return returns.size();
	}
	 
	/*
	 * this method is called to set the value of each cell
	 */
	@Override
	public Object getValueAt(int row, int column) {
		Return entity = null;
		entity= returns.get(row);
	 
		switch (column) {
			case 0:
				return entity.getId();
			case 1:
				return entity.getBookId();
			case 2:
				return entity.getReaderId();
			case 3:
				return entity.getReturnDate();
			case 4:
				return entity.getFineAmount();
			default :
		}
		 
		return "";
	}
	 
	 
	 //This method will be used to display the name of columns
	public String getColumnName(int col) {
		return headerList[col];
	}
}
