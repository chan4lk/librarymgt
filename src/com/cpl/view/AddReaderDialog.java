package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.cpl.data.PayType;
import com.cpl.service.IPaymentService;
import com.cpl.service.IReaderService;
import com.cpl.service.ServiceFactory;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This dialog will add a new book to the library.
 */
public class AddReaderDialog extends JDialog {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 2309200214777748119L;
	
	/**
	 * The empty string.
	 */
	private static final String emptyString = "";

	/**
	 * The default reader fee.
	 */
	private static final String defaultFee = "0.00";
	
	/**
	 * The content panel.
	 */
	private final JPanel contentPanel = new JPanel();
	
	/**
	 * The author text field.
	 */
	private JTextField fNameTxt;
	
	
	/**
	 * The publisher text field.
	 */
	private JTextField lNameTxt;
	
	/**
	 * Event to fire when book is added.
	 */
	private ReaderUpdateActionListener addEvent;
	private JTextField feeTxt;

	/**
	 * Create the dialog.
	 * @param event This event fires when book is added.
	 */
	public AddReaderDialog(ReaderUpdateActionListener event) {
		this.addEvent = event;
		setTitle("Add new Reader");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblFirstName = new JLabel("First Name");
			lblFirstName.setBounds(73, 40, 86, 21);
			contentPanel.add(lblFirstName);
		}
		
		fNameTxt = new JTextField();
		fNameTxt.setBounds(184, 32, 144, 36);
		contentPanel.add(fNameTxt);
		fNameTxt.setColumns(10);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(73, 93, 86, 21);
		contentPanel.add(lblLastName);
		
		lNameTxt = new JTextField();
		lNameTxt.setBounds(184, 85, 144, 36);
		contentPanel.add(lNameTxt);
		lNameTxt.setColumns(10);
		
		JLabel lblMemberFee = new JLabel("Member Fee");
		lblMemberFee.setBounds(73, 146, 86, 21);
		contentPanel.add(lblMemberFee);
		
		feeTxt = new JTextField();
		feeTxt.setBounds(184, 138, 144, 36);
		contentPanel.add(feeTxt);
		feeTxt.setColumns(10);
		feeTxt.setText(AddReaderDialog.defaultFee);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent event) {
						
						if (!fNameTxt.getText().equalsIgnoreCase("") &&
							!lNameTxt.getText().equalsIgnoreCase("") &&
							!feeTxt.getText().equalsIgnoreCase("")) {
							String firstName = fNameTxt.getText();
							String lastName = lNameTxt.getText();
							double fee = Double.parseDouble(feeTxt.getText());
							
							addNewReader(firstName, lastName, fee);
						}else{
							JOptionPane.showMessageDialog(
									AddReaderDialog.this,
									"Please Enter All nessary Fields",
									"Error", 
									JOptionPane.ERROR_MESSAGE);
						}
						
						
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						UIRenderer.hideAddReaderDialog();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	/**
	 * @param title of the book.
	 * @param author of the book.
	 * @param publisher of the book.
	 */
	protected void addNewReader(String firstName, String lastName, double fee) {
			IReaderService readerService = ServiceFactory.getReaderService();
			IPaymentService paymentService = ServiceFactory.getPaymentService();

			String id = readerService.addReader(firstName, lastName);
			
			if (!id.equalsIgnoreCase("")) {
				
				String payId = paymentService.addPayment("", id, fee, PayType.MemberFee, false);
				
				if (!payId.equalsIgnoreCase("")) {
					this.addEvent.onAdd();
					UIRenderer.hideAddReaderDialog();
				}				
			}
	}

	/**
	 * Clear all inputs.
	 */
	public void clearInputs() {
		this.fNameTxt.setText(emptyString);
		this.lNameTxt.setText(emptyString);
		this.feeTxt.setText(emptyString);		
	}
}
