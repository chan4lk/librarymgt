package com.cpl.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import com.cpl.service.BookMgtService;
import com.cpl.service.IBookMgtService;

/**
 * @author Chandima.Ranaweera
 * @version 1.0
 * This will list all the book to a data grid.
 */
public class BookFrame extends JFrame implements BookUpdateActionListener {

	/**
	 * The generated serial version id.
	 */
	private static final long serialVersionUID = 4780997252713603189L;
	
	/**
	 * The content pane.
	 */
	private JPanel contentPane;
	private JPanel panel;
	private JButton btnAddBook;
	private JButton btnUpdate;
	private JButton btnDelete;
	private JTable bookTable;
	private BookTableModel model;

	/**
	 * Create the frame.
	 */
	public BookFrame() {
		setTitle("Manage Books");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 1200, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		model = new BookTableModel(ViewType.ALL);
		bookTable = new JTable(model);
		
		JScrollPane scrollPane = new JScrollPane(bookTable);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.EAST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		btnAddBook = new JButton("Add Book");
		btnAddBook.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				UIRenderer.showAddBookDialog(BookFrame.this);				
			}
		});
		btnAddBook.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnAddBook);
		
		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				int selected = bookTable.getSelectedRow();
				UIRenderer.showUpdateBookDialog(selected, (BookUpdateActionListener)BookFrame.this);
			}
		});
		btnUpdate.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnUpdate);
		
		btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int selected = bookTable.getSelectedRow();
				IBookMgtService service = new BookMgtService();
				boolean success = service.deleteBook(selected);
				if (success) {
					BookFrame.this.onDelete();
					service.listBooks();
				}
			}
		});
		btnDelete.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(btnDelete);
		
	}
	
	/**
	 * initialize the frame.
	 */
	public void initialize(){
		
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onUpdate()
	 */
	@Override
	public void onUpdate() {
		System.out.println("On update Book Frame.");
		model.fireTableDataChanged();		
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onAdd()
	 */
	@Override
	public void onAdd() {
		System.out.println("On Add Book Frame.");
		model.fireTableDataChanged();		
	}

	/* (non-Javadoc)
	 * @see com.cpl.view.BookUpdateActionListener#onDelete()
	 */
	@Override
	public void onDelete() {
		model.fireTableDataChanged();		
	}

	@Override
	public void onSelect(String id) {
		// TODO Auto-generated method stub
		
	}

}
