package com.cpl.view;

/**
 * @author Chandima.Ranaweera
 *
 */
public interface BookUpdateActionListener {
	/**
	 * On book update this event will be fired.
	 */
	void onUpdate();
	
	/**
	 * When new book is added this event will be fired.
	 */
	void onAdd();
	
	/**
	 * When a book is deleted this event will be fired.
	 */
	void onDelete();

	/**
	 * @param selected The selected row.
	 */
	void onSelect(String bookId);
}
