package com.cpl.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.cpl.service.ISecurityService;
import com.cpl.service.SecurityService;

public class SecurityServiceTest {
	
	@Test
	 public void loginTest() {
	     // given
	     ISecurityService service = new SecurityService();
	     
	     // when
	    boolean granted = service.SignIn("User0", "123321");
	    
	     // then
	    assertTrue(granted);
	 }
}
