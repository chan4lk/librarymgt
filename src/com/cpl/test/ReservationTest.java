package com.cpl.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.cpl.data.Reservation;
import com.cpl.service.IReservationService;
import com.cpl.service.ReservationService;

public class ReservationTest {

	@Test
	public void reservationListTest() {
		IReservationService service = new ReservationService();
		
		service.listReservations();
	}
	
	@Test
	public void reserveTest() throws Exception {
		IReservationService service = new ReservationService();
		
		String id = service.reserveBook("Title 2", "R  1");
		
		Reservation res = service.GetById(id);
		
		assertNotNull(res);
	}

}
