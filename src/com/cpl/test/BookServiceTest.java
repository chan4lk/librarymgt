package com.cpl.test;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Test;

import com.cpl.data.Book;
import com.cpl.service.BookMgtService;
import com.cpl.service.IBookMgtService;
import com.cpl.service.ServiceFactory;

public class BookServiceTest {

	@Test
	public void getBookByIdTest() throws NoSuchElementException {
		IBookMgtService service = new BookMgtService();
		
		service.listBooks();
		
		Book book = service.getById(String.format("B%3d", 1));
		
		assertNotNull(book);
	}
	
	@Test
	public void addBookTest() {
		IBookMgtService service = new BookMgtService();
		
		Book book = new Book();
		book.setAuthor("Author connen doyle");
		book.setTitle("Sherlock Holmes");
		book.setPublisher("London Times");
		book.setBorrowed(false);
		book.setDeleted(false);
		book.setReserved(false);
		
		//String id = service.addbook(book);
		
		//assertEquals("", id);
	}
	
	@Test
	public void deleteBookTest() throws NoSuchElementException {
		IBookMgtService service = ServiceFactory.getBookMgtService();
		service.deleteBook(1);
		Book book = service.getById("B  2");
		service.listBooks();
		assertTrue(book.isDeleted());
	}
	

}
